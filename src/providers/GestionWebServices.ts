import { ListaAlarmas } from './Estadisticos/ListaAlarmas';
import { ListaInfra } from './Estadisticos/ListaInfra';
import { NocDetalle } from './Tickets/NocDetalle';
import { CareDetalle } from './Tickets/CareDetalle';
import { CareGeneral } from './Tickets/CareGeneral';
import { NocGeneral } from './Tickets/NocGeneral';
import { Router } from './Estadisticos/Router';
import { Olt } from './Estadisticos/Olt';
import { Switch } from './Estadisticos/Switch';
import { Colector } from './Estadisticos/Colector';
import { Http, Headers, RequestOptions } from '@angular/http';


import { Usuario } from './Usuario';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Organizacion } from './Organizacion';
import { Estadisticos } from './Estadisticos';
import { Ticket } from './Tickets/Ticket';
import { CareNocInformation } from './Tickets/CareNocInformation';
import { ListaGenerica } from './InfraEstructura/ListaGenerica';
import { Keychain } from '@ionic-native/keychain';
import { CommonUtilsProvide } from './Utils/CommonUtilsProvide';
import { SessionVariables } from './Session/SessionVariables';


@Injectable()
export class GestionWebServices {
  
  //=====================Para probar en el Navegador=====================
  //path = "";

  //=====================Para probar en iOS=====================
  path = "http://159.65.102.33";
  
  
  constructor(
          public http: Http,
          
          public estadisticos:Estadisticos,
          public listasInfra:ListaInfra,
          private keychain: Keychain,
          private encrypt:CommonUtilsProvide,
          private sessionVariables:SessionVariables
        ) {
    this.http = http
  }
  public acceso(credenciales) {
    console.log("acceso - GestionWebServices")
     if (credenciales.email === null || credenciales.password === null) {
       return Observable.throw("Porfavor inserta las credenciales");
     } else {
       return Observable.create(observer => {
       
         this.keychain.get("KEY_USER_APP_ALARMAS").then(value =>{
 
             let data = {
                 'username': credenciales.usuario,
                 'password': this.encrypt.encryptPayload(credenciales.contrasenia),
                 'tokenDevice': credenciales.tokenDevice,
                 'uuid':value
             };
 
             let headers = new Headers();
               headers.append('Content-Type', 'application/json');
               headers.append( "postman-token", "38bbd3d5-66e9-f3b6-33ff-12c663b68490");
     
             let options = new RequestOptions({ headers: headers });
     
             this.http.post(
               this.path+'/serviciosrest/login',data ,options).subscribe(data => {
                 if (data.status==200) {
 
                   let dataR = data.json();
                   this.sessionVariables.tokenUser = dataR.token
                   let currentUser = new Usuario();
                     currentUser.userName = credenciales.usuario
                     currentUser.password = credenciales.contrasenia
                     currentUser.access_token = dataR.token
                     currentUser.tipoUsuario = dataR.tipo
                     currentUser.alarmas = dataR.alarmas
 
                   let dataResponse = {status:200,user:currentUser}
                     observer.next(dataResponse);
                     observer.complete();
                 }
                 else{
                   observer.next(data);
                   observer.complete();
                 }
               }, error => {
                 let dataR = error.json();
                   observer.next(dataR);
                   observer.complete();
               });
           
           })
           .catch(err => {
             observer.next(err);
             observer.complete();
           });
         
       });
     }
  }
  
  public agregaUsuario(information) {
    console.log("agregaUsuario - GestionWebServices")
    

    
    return Observable.create(observer => {
      this.keychain.set("KEY_USER_APP_ALARMAS", information.uuid,false).then(() => {
        let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
          
        let options = new RequestOptions({ headers: headers });
        this.http.post(
          this.path+'/serviciosrest/usuarios/create/',information ,options).subscribe(data => {
            let dataR = data.json();
            if (dataR.status == true) {
              let currentUser = new Usuario();
                currentUser.id = dataR.userId
                currentUser.userName = information.username
                currentUser.nombreUsuario = information.nombreUsuario
                currentUser.apellidoUsuario = information.apellidoUsuario
              let organizacion = new Organizacion();
                organizacion.nombre = information.organizacion
                currentUser.organizacion = organizacion
                currentUser.tipoUsuario = information.tipoUsuario
                currentUser.alarmas = information.alarmas
              let dataResponse = {status:200,user:currentUser}
                observer.next(dataResponse);
                observer.complete();
            }else{
              let dataResponse = {status:false,error:dataR.error}
              observer.next(dataResponse);
              observer.complete();
            }
          }, error => {
            let dataResponse = {status:false}
              observer.next(dataResponse);
              observer.complete();
          });
      
      }).catch(err => {
        let dataResponse = {status:false}
        observer.next(dataResponse);
        observer.complete();
      });
    });
}
  public getAllUsers() {
    return Observable.create(observer => {
      let headers = new Headers();
        headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });
      
      this.http.get(
          this.path+"/serviciosrest/usuarios/",options).subscribe(data => {
           // "/serviciosrest/usuarios/",{}).subscribe(data => {
            let dataR = data.json();
            console.log("Informacion Usuarios")
            console.log(dataR)
            console.log(dataR.length)
            let dataResponse = {}
            if(dataR.length != 0){
              dataResponse = {status:true,users:dataR}
            }else{
              dataResponse = {status:false}
            }
            observer.next(dataResponse);
            observer.complete();
          }, error => {
            let dataResponse = {status:false}
            observer.next(dataResponse);
            observer.complete();
        });
      
    });
  }
  public getPermisosUser(idUser) {
    return Observable.create(observer => {
        let headers = new Headers();
        headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
        let options = new RequestOptions({ headers: headers });
        this.http.get(
          
             "/serviciosrest/usuarios/permisos/"+idUser,options).subscribe(data => {
            let dataR = data.json();
            console.log("Informacion Usuarios")
            console.log(dataR)

            let dataResponse = {}


            dataResponse = {status:true,permisos:dataR}

            observer.next(dataResponse);
            observer.complete();
          }, error => {
            let dataResponse = {status:false}
            observer.next(dataResponse);
            observer.complete();
        });
    });
  }
  public getEstadisticas() {
    console.log("getEstadisticas - GestionWebServices")

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/devices",options).subscribe(data => {
              if(data.status==200){
                //"/serviciosrest/estadisticas/devices",{}).subscribe(data => {
                let dataR = data.json();
                let colector = new Colector(dataR.colectores[0]);
                let olt = new Olt(dataR.olts[0]);
                let router = new Router(dataR.router[0]);
                let switx = new Switch(dataR.switches[0]);
                this.estadisticos.colector = colector;
                this.estadisticos.olt = olt;
                this.estadisticos.router = router;
                this.estadisticos.switch = switx;
                let dataResponse = {status:200,estadisticos: this.estadisticos}
                observer.next(dataResponse);
                observer.complete();
              }
              else{
                observer.next(data);
                observer.complete();
              }
          }, error => {
            observer.next(error);
            observer.complete();
        });
      });
  }
  public getEstadisticasInfra() {
    console.log("getEstadisticasInfra - GestionWebServices");

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/infraestructura/totales",options).subscribe(data => {
              if(data.status==200){
                
                
                  let dataR = data.json();
                  let listDispositivos = new ListaGenerica();
                    listDispositivos.setInfraTotales(dataR);

                  let dataResponse = {status:200,estadisticos: listDispositivos}
                  observer.next(dataResponse);
                  observer.complete();
               
              }
              else{
                observer.next(data);
                observer.complete();
              }
          }, error => {
            observer.next(error);
            observer.complete();
        });
      
    });
  }
  public getEstadisticasInfraDetalle() {

    console.log("getEstadisticasInfraDetalle - GestionWebServices");
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

      this.http.get(
          this.path+"/serviciosrest/estadisticas/infraestructura/detalles",options).subscribe(data => {
            if (data.status==200) {
              let dataR = data.json();
              this.listasInfra.critica(dataR["critica"]);
              this.listasInfra.media(dataR["media"]);
              this.listasInfra.baja(dataR["baja"]);
              let dataResponse = {status:200,listasInfra: this.listasInfra}
              observer.next(dataResponse);
              observer.complete();
            }else{
              observer.next(data);
              observer.complete();
            }
        }, error => {
          observer.next(error);
          observer.complete();
      });

    });
  }
  public updateUsuario(id,information) {
          console.log("updateUsuario - GestionWebServices");
          return Observable.create(observer => {
            let headers = new Headers();
              headers.append('Content-Type', 'application/json');
              headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
              let options = new RequestOptions({ headers: headers });

            this.http.post(
              this.path+'/serviciosrest/usuarios/update/'+id+"/",information ,options).subscribe(data => {
                let dataR = data.json();
                if(dataR.result = true){
                  let dataResponse = {status:true}
                  observer.next(dataResponse);
                  observer.complete();
                }else{
                  let dataResponse = {status:false,error:dataR.error}
                  observer.next(dataResponse);
                  observer.complete();
                }
          
           }, error => {
            let dataResponse = {status:false}
              observer.next(dataResponse);
              observer.complete();
          });

          });

      }

  public updateUsuarioWithKeyChain(id,information) {
    console.log("updateUsuarioWithKeyChain - GestionWebServices");
    return Observable.create(observer => {
      this.keychain.set("KEY_USER_APP_ALARMAS",information.uuid,false).then(value =>{
        let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
          let options = new RequestOptions({ headers: headers });
        this.http.post(
          this.path+'/serviciosrest/usuarios/update/'+id+"/",information ,options)
          .subscribe(data => {
            let dataR = data.json();
            if(dataR.result = true){
              let dataResponse = {status:true}
              observer.next(dataResponse);
              observer.complete();
            }else{
              let dataResponse = {status:false,error:dataR.error}
              observer.next(dataResponse);
              observer.complete();
            }
  
          }, error => {
            let dataResponse = {status:false}
              observer.next(dataResponse);
              observer.complete();
          });
      }).catch(err => {
        observer.next(err);
        observer.complete();
      });
    });
  }
  public deleteUsuario(id) {

    return Observable.create(observer => {
      
      let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
        

      let options = new RequestOptions({ headers: headers });

      this.http.post(
        this.path+'/serviciosrest/usuarios/delete/'+id+"/",{} ,options).subscribe(data => {
          

      let dataR = data.json();


      console.log(dataR)
      let dataResponse = {status:true}
        observer.next(dataResponse);
        observer.complete();
      }, error => {
      let dataResponse = {status:false}
        observer.next(dataResponse);
        observer.complete();
    });

    });

  }

  public getTickets() {
    console.log("getTickets - GestionWebServices");

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/ticketsGeneral/",options).subscribe(data => {
            if (data.status==200) {
              let dataR = data.json();
              let ticketCare =   new Ticket();
              ticketCare.setValues(dataR.care,"CARE","grafica1")
              let ticketNoc =   new Ticket();
              ticketNoc.setValues(dataR.noc,"NOC","grafica2")

              let arrayElements = []
              arrayElements.push(ticketNoc);
              arrayElements.push(ticketCare);
              let dataResponse = {status:200,arrayElements:arrayElements}
              observer.next(dataResponse);
              observer.complete();
            }else{
              observer.next(data);
              observer.complete();
            }
          }, error => {
            observer.next(error);
            observer.complete();
        });
    });
  }
  public getNocGeneral() {
    console.log("getNocGeneral - GestionWebServices");
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/noc/informationGeneral",options).subscribe(data => {
              if (data.status==200 && data.json().success == true) {
                console.log("---------------------------Servicio NocGeneral OK "+data)
                let dataR = data.json();
                let nocGeneral =   new NocGeneral();
                nocGeneral.setValues(dataR)
                let dataResponse = {status:200,nocGeneral:nocGeneral}
                observer.next(dataResponse);
                observer.complete();
              }else{
                console.log("---------------------------Servicio NocGeneral !OK "+data)
                data.status = 500;
                observer.next(data);
                observer.complete();
              }
          }, error => {
            console.log("---------------------------Servicio NocGeneral Error "+error)
            observer.next(error);
            observer.complete();
        });
    });
  }

  public getCareGeneral() {
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/care/informationGeneral",options).subscribe(data => {
              if (data.status==200) {
                let dataR = data.json();
                let careGeneral =   new CareGeneral()
                careGeneral.setValues(dataR)
                let dataResponse = {status:200,careGeneral:careGeneral}
                observer.next(dataResponse);
                observer.complete();
              }else{
                observer.next(data);
                observer.complete();
              }
          }, error => {
            observer.next(error);
            observer.complete();
        });
    });
  }
  public getCareDetalle() {
    console.log("getCareDetalle - GestionWebServices")

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });
        this.http.get(
            this.path+"/serviciosrest/estadisticas/care/detalle",options).subscribe(data => {
              if (data.status==200 && data.json().success == true) {
                let dataR = data.json();

                let careDetalle =   new CareDetalle();
                careDetalle.setValues(dataR)
                let dataResponse = {status:200,careDetalle:careDetalle}
                observer.next(dataResponse);
                observer.complete();
              }else{

                observer.next(data);
                observer.complete();
                data.status = 500;
              }
          }, error => {
            observer.next(error);
            observer.complete();
        });
    });
  }

  public getNocDetalleBack() {
    console.log("getNocDetalleBack - GestionWebServices")

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/noc/detalle/back",options).subscribe(data => {
              if (data.status==200 && data.json().success == true) {
                let dataR = data.json();
                let careDetalle =   new NocDetalle();
                careDetalle.setValuesBack(dataR)
                let dataResponse = {status:200,careDetalle:careDetalle}
                observer.next(dataResponse);
                observer.complete();
              }else{
                data.status = 500;
                observer.next(data);
                observer.complete();
              }
          }, error => {
            observer.next(error);
            observer.complete();
        });
    });
  }

  public getNocDetalleFront() {
    console.log("getNocDetalleFront - GestionWebServices")

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

        this.http.get(
            this.path+"/serviciosrest/estadisticas/noc/detalle/front",options).subscribe(data => {
              if (data.status==200 && data.json().success == true) {
                let dataR = data.json();
                console.log(dataR);
                let nocDetalleFront =   new NocDetalle();
                nocDetalleFront.setValuesFront(dataR)
                console.log(nocDetalleFront);
                let dataResponse = {status:200,nocDetalleFront:nocDetalleFront}
                observer.next(dataResponse);
                observer.complete();
              }else{
                data.status = 500;
                observer.next(data);
                observer.complete();
              }
          }, error => {
            observer.next(error);
            observer.complete();
        });
    });
  }

  public getListaAlarmasOlt() {
    console.log("getListaAlarmasOlt - GestionWebServices");
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

      this.http.get(
          this.path+"/serviciosrest/estadisticas/listaAlarmas/olt",options).subscribe(data => {
            if (data.status==200) {
              let dataR = data.json();
              let lAlarmas = new ListaAlarmas(dataR);
              observer.next(lAlarmas.listaAlarmas);
              observer.complete();
            }else{
              observer.next(data);
              observer.complete();
            }
        }, error => {
          observer.next(error);
          observer.complete();
      });

    });
  }

  public getListaAlarmasRouter() {
    console.log("getListaAlarmasRouter - GestionWebServices");

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

      this.http.get(
          this.path+"/serviciosrest/estadisticas/listaAlarmas/router",options).subscribe(data => {
            if (data.status==200) {
              let dataR = data.json();
              let lAlarmas = new ListaAlarmas(dataR);
              observer.next(lAlarmas.listaAlarmas);
              observer.complete();
            }else{
              observer.next(data);
              observer.complete();
            }
        }, error => {
          observer.next(error);
          observer.complete();
      });

    });
  }
  public getListaAlarmasSwitchs() {
    console.log("getListaAlarmasSwitchs - GestionWebServices");
    let headers = new Headers();
    headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
    let options = new RequestOptions({ headers: headers });

    return Observable.create(observer => {

      this.http.get(
        
            this.path+"/serviciosrest/estadisticas/listaAlarmas/switchs",options).subscribe(data => {
              if (data.status==200) {
                let dataR = data.json();
                let lAlarmas = new ListaAlarmas(dataR);
                observer.next(lAlarmas.listaAlarmas);
                observer.complete();
              }else{
                observer.next(data);
                observer.complete();
              }
        }, error => {
          observer.next(error);
          observer.complete();
      });

    });
  }

  public getListaAlarmasColectores() {
    console.log("getListaAlarmasColectores - GestionWebServices");

    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      let options = new RequestOptions({ headers: headers });

      this.http.get(
            this.path+"/serviciosrest/estadisticas/listaAlarmas/colectores",options).subscribe(data => {
              if (data.status==200) {
                let dataR = data.json();
                let lAlarmas = new ListaAlarmas(dataR);
                observer.next(lAlarmas.listaAlarmas);
                observer.complete();
              }else{
                observer.next(data);
                observer.complete();
              }
        }, error => {
          observer.next(error);
          observer.complete();
      });

    });
  }

  public setTokenDevice(information) {
    console.log("setTokenDevice - GestionWebServices");
    return Observable.create(observer => {

      let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
        let options = new RequestOptions({ headers: headers });

      this.http.post(
        this.path+'/serviciosrest/usuarios/assignToken/',information ,options).subscribe(data => {
        if (data.status==200 && data.json().status == true) {
          let dataResponse = {status:true}
          observer.next(dataResponse);
          observer.complete();
        }else{
          let dataResponse = {status:false}
          observer.next(dataResponse);
          observer.complete();
        }           
      }, error => {
        let dataResponse = {status:false}
        observer.next(dataResponse);
        observer.complete();
    });

    });

  }


  public getInformationNocCare(information) {

      return Observable.create(observer => {
        let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
          
        let options = new RequestOptions({ headers: headers });

        this.http.post(
          this.path+'/serviciosrest/estadisticas/nocCare/detalle/',information ,options).subscribe(data => {
            let responseBody = new CareNocInformation(data.json())
            let dataResponse = {status:true,data:responseBody}
            observer.next(dataResponse);
            observer.complete();
        }, error => {
        let dataResponse = {status:false}
          observer.next(dataResponse);
          observer.complete();
      });

      });

  }

  public getInformationInfraEquipos(information) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
    let options = new RequestOptions({ headers: headers });

    return Observable.create(observer => {

      this.http.post(
        this.path+'/serviciosrest/estadisticas/infraestructura/equipos/',information ,options).subscribe(data => {
          
          let responseBody = new ListaGenerica();
            responseBody.setInfraEquipos(data.json());
          let dataResponse = {status:true,data:responseBody}
          observer.next(dataResponse);
          observer.complete();
      }, error => {
      let dataResponse = {status:false}
        observer.next(dataResponse);
        observer.complete();
    });

    });

  }

  public getInformationInfraAlarmas(information) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
      
    let options = new RequestOptions({ headers: headers });

      return Observable.create(observer => {
        this.http.post(
          this.path+'/serviciosrest/estadisticas/infraestructura/alarmas/',information ,options).subscribe(data => {
            let responseBody = new ListaGenerica();
              responseBody.setInfraAlarmas(data.json());
            let dataResponse = {status:true,data:responseBody}
            observer.next(dataResponse);
            observer.complete();
       }, error => {
        let dataResponse = {status:false}
          observer.next(dataResponse);
          observer.complete();
      });

      });

  }

  public getFiltroAlarmasSaludRed(information) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Token '+this.sessionVariables.tokenUser);
    let options = new RequestOptions({ headers: headers });

    return Observable.create(observer => {
      this.http.post(
        this.path+'/serviciosrest/estadisticas/saludAlarmas/',information ,options).subscribe(data => {
          
          if (data.status==200) {
            let dataR = data.json();
            let lAlarmas = new ListaAlarmas(dataR);
            observer.next(lAlarmas.listaAlarmas);
            observer.complete();
          }else{
            observer.next(data);
            observer.complete();
          }
     }, error => {
      let dataResponse = {status:false}
        observer.next(dataResponse);
        observer.complete();
    });

    });

  }

}

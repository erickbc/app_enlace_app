import { Organizacion } from './Organizacion';
export class Usuario {
    id:string;
    userName: string;
    nombreUsuario:string;
    apellidoUsuario:string;
    password: string;
    access_token:string;
    token_type:string;
    refresh_token:string;
    organizacion:Organizacion;
    tipoUsuario:string;

    alarmas:boolean;

    constructor() {}



  }
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as CryptoJS from 'crypto-js';

import { ToastController, Toast } from 'ionic-angular';
import { AlertController, Alert } from 'ionic-angular';
import { CommonVariablesProvider } from './CommonVariablesProvider';

/*
 *  Common Utilities provider
 *  @author Alfred Francis
 *  @date 10/06/2017
*/
@Injectable()
export class CommonUtilsProvide {
  toast: Toast
  alert: Alert
  constructor(
              public commonVariablesProvider:CommonVariablesProvider,
              public toastController: ToastController,
              private alertCtrl: AlertController
             )
  {
    console.log('common utils loaded');
  }

    storeUserInfo(userInfo){
    /*
     * Store user information in localStorage
     * @param {json}  userInfo - user details to store
     * @return {}
     */
    localStorage.setItem('currentUser', JSON.stringify(userInfo));
    }
    getUserInfo(){
    /*
     * get user information from localStorage
     * @return {json}
     */
        if (!localStorage.getItem('currentUser')) {
            // userInfo not set so return false
            return false;
        }else{
            // parse and return userInfo
            return (JSON.parse(localStorage.getItem('currentUser')))
        }
    }
    encryptPayload(plainPayload){
      /*
       * Encrypt payload
       * @param {string}  plainPassword - user details to store
       * @return {}
       */
  
      // msgString is expected to be Utf8 encoded
      let key = CryptoJS.enc.Utf8.parse(this.commonVariablesProvider.commonAESKey);

      var iv = CryptoJS.lib.WordArray.random(16);

      var encrypted = CryptoJS.AES.encrypt(plainPayload, key, {
        iv: iv
      });
      return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
    }

    encryptPassword(plainPassword){
    /*
     * Encrypt password
     * @param {string}  plainPassword - user details to store
     * @return {}
     */

    // msgString is expected to be Utf8 encoded
    let key = CryptoJS.enc.Utf8.parse(this.commonVariablesProvider.commonAESKey);
    var iv =  CryptoJS.enc.Utf8.parse(this.commonVariablesProvider.passwordIV);;
    var encrypted = CryptoJS.AES.encrypt(plainPassword, key, {
      iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
  }

  decryptPayload(ciphertextStr) {
    /*
     * Decrypt payload
     * @param {string}  ciphertextStr - Encrypted Payload
     * @return {}
     */

    let key = CryptoJS.enc.Utf8.parse(this.commonVariablesProvider.commonAESKey);
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
      iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

    notify(message: string) {
    /**
     * For showing notification to user
     * @param {string}  message - Message to be shown
     * @return {Toast}
     */
      try {
        this.toast.dismiss();
      } catch (e) { }
      this.toast = this.toastController.create({
        message: message,
        position: 'bottom',
        duration: 3000,
        showCloseButton: true
      });
      this.toast.present()
      return this.toast
  }

  getConfirmation(title: string, message: string) {
    /**
     * For showing notification to user
     * @param {string}  title - title for Prompt
     * @param {string}  message - Message body
     * @return {Alert}
     */
    return new Promise(
      (resolve, reject) => {
        this.alert = this.alertCtrl.create({
          title: title,
          message: message,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                reject('cancel')
              }
            },
            {
              text: 'Ok',
              handler: () => {
                resolve('ok')
              }
            }
          ]
        })
        this.alert.present()
      }
    )

  }
}
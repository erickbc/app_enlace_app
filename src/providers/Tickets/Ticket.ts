import { Niveles } from './Niveles';
import { Injectable } from "@angular/core";

@Injectable()
export class Ticket {
  public nombre = "";
  public totalVencidos="0";
  public totalXVencer="0";
  public totalAbiertos="0";
  public idGrafica:String;

  public arrayTotales={media: new Niveles(0,null), alta: new Niveles(1,null), critica: new Niveles(2,null)};


  public status=true


  constructor() {
  }






  setValues(json,name,grafica) {
    this.nombre = name;
    this.idGrafica = grafica
    try {
      this.totalVencidos = json.totalesGenerales.vencidos
      this.totalXVencer = json.totalesGenerales.xVencer
      this.totalAbiertos = json.totalesGenerales.abiertos

    } catch (error) {

    }

    try {
      let values = json.totales[0]
    //Media
    this.arrayTotales["media"]=(new Niveles(0,values.media));
    //Alta
    this.arrayTotales["alta"]=(new Niveles(1,values.alta));
    //Critica
    this.arrayTotales["critica"]=(new Niveles(2,values.critica));
    } catch (error) {
      this.arrayTotales["media"]=(new Niveles(0,null));
    //Alta
      this.arrayTotales["alta"]=(new Niveles(1,null));
    //Critica
      this.arrayTotales["critica"]=(new Niveles(2,null));

      this.status = false
    }





  }
}


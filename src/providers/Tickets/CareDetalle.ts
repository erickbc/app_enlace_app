import { Niveles } from './Niveles';
import { Injectable } from "@angular/core";

@Injectable()
export class CareDetalle {

  public monitoreo={};
  public estrategico={};
  public gobierno={};

  constructor() {
  }

  setValues(json) {
    console.log("=====SET VALUES CARE GENERAL=========");
    console.log(json);
    //Media - Monitoreo

    let arrayMonitoreoMedia = [];
    json.monitoreo.media.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayMonitoreoMedia.push(n);
    });

    let arrayMonitoreoAlta = [];
    json.monitoreo.alta.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayMonitoreoAlta.push(n);
    });

    let arrayMonitoreoCritica = [];
    json.monitoreo.critica.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayMonitoreoCritica.push(n);
    });

    this.monitoreo["media"]=arrayMonitoreoMedia;
    //Alta - Monitoreo
    this.monitoreo["alta"]=arrayMonitoreoAlta;
    //Critica - Monitoreo
    this.monitoreo["critica"]=arrayMonitoreoCritica;
    //Identificador - Monitoreo
    this.monitoreo["nombre"] = "Care Monitoreo"


    let arrayEstrategicoMedia = [];
    json.estrategico.media.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayEstrategicoMedia.push(n);
    });

    let arrayEstrategicoAlta = [];
    json.estrategico.alta.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayEstrategicoAlta.push(n);
    });

    let arrayEstrategicoCritica = [];
    json.estrategico.critica.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayEstrategicoCritica.push(n);
    });


    //Media - Estrategicos
    this.estrategico["media"]=arrayEstrategicoMedia;
    //Alta - Estrategicos
    this.estrategico["alta"]=arrayEstrategicoAlta;
    //Critica - Estrategicos
    this.estrategico["critica"]=arrayEstrategicoCritica;
    //Identificador - Estrategicos
    this.estrategico["nombre"] = "Care Estrategico"


    let arrayGobiernoMedia = [];
    json.gobierno.media.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayGobiernoMedia.push(n);
    });

    let arrayGobiernoAlta = [];
    json.gobierno.alta.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayGobiernoAlta.push(n);
    });

    let arrayGobiernoCritica = [];
    json.gobierno.critica.forEach(element => {
       let n =  new Niveles(0,element);
        n.categoria = element.categoria;
        arrayGobiernoCritica.push(n);
    });


    //Media - Gobierno
    this.gobierno["media"]=arrayGobiernoMedia
    //Alta - Gobierno
    this.gobierno["alta"]=arrayGobiernoAlta
    //Critica - Gobierno
    this.gobierno["critica"]=arrayGobiernoCritica
    //Identificador - Gobierno
    this.gobierno["nombre"] = "Care Gobierno"

  }
}

import { Niveles } from './Niveles';
import { Injectable } from "@angular/core";

@Injectable()
export class NocDetalle {

  public frontOffice={};
  public backOffice={};

  constructor() {
  }






  setValuesBack(json) {
    console.log(" =====SET VALUES BACK======")
    console.log(" =====JSON TO PARSE======")
    console.log(json.backOffice)



    let arrayMonitoreoMedia = [];
    let arrayMonitoreoAlta = [];
    let arrayMonitoreoCritica = [];

    json.backOffice.forEach(elementArray => {
     try {

          elementArray.backOfficeMedia.forEach(element => {
          let n =  new Niveles(0,element);
            n.categoria = element.categoria;
            arrayMonitoreoMedia.push(n);
          });
     } catch (error) {

     }
     try {

          elementArray.backOfficeCritica.forEach(element => {
            let n =  new Niveles(2,element);
              n.categoria = element.categoria;
              arrayMonitoreoCritica.push(n);
          });
     } catch (error) {

     }

     try {
          elementArray.backOfficeAlta.forEach(element => {
            let n =  new Niveles(1,element);
              n.categoria = element.categoria;
              arrayMonitoreoAlta.push(n);
        });
     } catch (error) {

     }
    });







    console.log(" =====SET VALUES BACK======");
    console.log(arrayMonitoreoMedia);
    console.log(arrayMonitoreoAlta);
    console.log(arrayMonitoreoCritica);




    //Media - FrontOffice
    this.backOffice["media"]=arrayMonitoreoMedia;
    //Alta - FrontOffice
    this.backOffice["alta"]=arrayMonitoreoAlta;
    //Critica - FrontOffice
    this.backOffice["critica"]=arrayMonitoreoCritica;
    //Identificador - FrontOffice
    this.backOffice["nombre"] = "Back Office"



  }

  setValuesFront(json) {
    console.log(" =====SET VALUES FRONT======")
    console.log(" =====JSON TO PARSE======")
    console.log(json.backOffice)


    let arrayMonitoreoMedia = [];
    let arrayMonitoreoAlta = [];
    let arrayMonitoreoCritica = [];

    json.frontOffice.forEach(elementArray => {
     try {

          elementArray.frontOfficeMedia.forEach(element => {
          let n =  new Niveles(0,element);
            n.categoria = element.categoria;
            arrayMonitoreoMedia.push(n);
          });
     } catch (error) {

     }
     try {

          elementArray.frontOfficeCritica.forEach(element => {
            let n =  new Niveles(2,element);
              n.categoria = element.categoria;
              arrayMonitoreoCritica.push(n);
          });
     } catch (error) {

     }

     try {
          elementArray.frontOfficeAlta.forEach(element => {
            let n =  new Niveles(1,element);
              n.categoria = element.categoria;
              arrayMonitoreoAlta.push(n);
        });
     } catch (error) {

     }
    });







    console.log(" =====SET VALUES FRONT======");
    console.log(arrayMonitoreoMedia);
    console.log(arrayMonitoreoAlta);
    console.log(arrayMonitoreoCritica);




    //Media - FrontOffice
    this.frontOffice["media"]=arrayMonitoreoMedia;
    //Alta - FrontOffice
    this.frontOffice["alta"]=arrayMonitoreoAlta;
    //Critica - FrontOffice
    this.frontOffice["critica"]=arrayMonitoreoCritica;
    //Identificador - FrontOffice
    this.frontOffice["nombre"] = "Back Office"



  }

}


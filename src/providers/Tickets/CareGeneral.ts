import { Niveles } from './Niveles';
import { Injectable } from "@angular/core";

@Injectable()
export class CareGeneral {

  public monitoreo={};
  public estrategico={};
  public gobierno={};

  constructor() {
  }






  setValues(json) {
    console.log("=====SET VALUES CARE GENERAL=========");
    console.log(json);
    //Media - Monitoreo
    this.monitoreo["media"]=(new Niveles(0,json.monitoreo.media));
    //Alta - Monitoreo
    this.monitoreo["alta"]=(new Niveles(1,json.monitoreo.alta));
    //Critica - Monitoreo
    this.monitoreo["critica"]=(new Niveles(2,json.monitoreo.critica));
    //Identificador - Monitoreo
    this.monitoreo["nombre"] = "Care Monitoreo"

    //Media - Estrategicos
    this.estrategico["media"]=(new Niveles(0,json.estrategicos.media));
    //Alta - Estrategicos
    this.estrategico["alta"]=(new Niveles(1,json.estrategicos.alta));
    //Critica - Estrategicos
    this.estrategico["critica"]=(new Niveles(2,json.estrategicos.critica));
    //Identificador - Estrategicos
    this.estrategico["nombre"] = "Care Estratégico"

    //Media - Gobierno
    this.gobierno["media"]=(new Niveles(0,json.gobierno.media));
    //Alta - Gobierno
    this.gobierno["alta"]=(new Niveles(1,json.gobierno.alta));
    //Critica - Gobierno
    this.gobierno["critica"]=(new Niveles(2,json.gobierno.critica));
    //Identificador - Gobierno
    this.gobierno["nombre"] = "Care Gobierno"

  }
}

import { Niveles } from './Niveles';
import { Injectable } from "@angular/core";

@Injectable()
export class NocGeneral {

  public frontOffice={};
  public backOffice={};

  constructor() {
  }






  setValues(json) {
    //Media - FrontOffice
    this.frontOffice["media"]=(new Niveles(0,json.frontOffice[0].media));
    //Alta - FrontOffice
    this.frontOffice["alta"]=(new Niveles(1,json.frontOffice[0].alta));
    //Critica - FrontOffice
    this.frontOffice["critica"]=(new Niveles(2,json.frontOffice[0].critica));
    //Identificador - FrontOffice
    this.frontOffice["nombre"] = "Front Office"

    //Media - BackOffice
    this.backOffice["media"]=(new Niveles(0,json.backOffice[0].media));
    //Alta - BackOffice
    this.backOffice["alta"]=(new Niveles(1,json.backOffice[0].alta));
    //Critica - BackOffice
    this.backOffice["critica"]=(new Niveles(2,json.backOffice[0].critica));
    //Identificador - BackOffice
    this.backOffice["nombre"] = "Back Office"

  }
}


import { Injectable } from "@angular/core";
import { DispositivoInfra } from './DispositivoInfra';
@Injectable()
export class ListaInfra {
    nivel:string;
    listaInfra =[];
    listaMedia=[];
    listaBaja=[];
    listaCritica=[];


  constructor() {

  }
  media(json){
    this.listaMedia=[];
    this.nivel="Media"
    if(json.shelter.length != 0){
      this.listaMedia.push(new DispositivoInfra(json.shelter,"Shelter"))
    }
    if(json.jaguar.length != 0){
      this.listaMedia.push(new DispositivoInfra(json.jaguar,"Jaguar"))
    }
    if(json.delta.length != 0){
      this.listaMedia.push(new DispositivoInfra(json.delta,"Delta"))
    }
    if(json.sheterLD.length != 0){
      this.listaMedia.push(new DispositivoInfra(json.sheterLD,"Shelter LD"))
    }

    }
  baja(json){
    this.listaBaja=[];
    this.nivel="Baja"
    if(json.shelter.length != 0){
      this.listaBaja.push(new DispositivoInfra(json.shelter,"Shelter"))
    }
    if(json.jaguar.length != 0){
      this.listaBaja.push(new DispositivoInfra(json.jaguar,"Jaguar"))
    }

    if(json.delta.length != 0){
      this.listaBaja.push(new DispositivoInfra(json.delta,"Delta"))
    }
    if(json.sheterLD.length != 0){
    this.listaBaja.push(new DispositivoInfra(json.sheterLD,"Shelter LD"))
    }
  }
  critica(json){
    this.listaCritica=[];
    this.nivel="Critica"
    if(json.shelter.length != 0){
      this.listaCritica.push(new DispositivoInfra(json.shelter,"Shelter"))
    }
    if(json.jaguar.length != 0){
      this.listaCritica.push(new DispositivoInfra(json.jaguar,"Jaguar"))
    }
    if(json.delta.length != 0){
      this.listaCritica.push(new DispositivoInfra(json.delta,"Delta"))
    }
    if(json.sheterLD.length != 0){
      this.listaCritica.push(new DispositivoInfra(json.sheterLD,"Shelter LD"))
    }
  }
}

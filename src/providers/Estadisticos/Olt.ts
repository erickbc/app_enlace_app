

export class Olt {
  nombre = "OLTS";
  id:string = "0";
  down: string= "0";
  temperatura:string= "0";
  fallaOptica:string= "0";
  memoria: string= "0";
  voltaje:string= "0";
  total:string= "0";
  cpu:string= "0";
  ventilador:string= "0";
  fallaGestion:string= "0";
  disponibilidad:string= "0";

  constructor(json) {

    try {
      this.id = json.id;
      this.cpu = json.cpuOlt;
      this.total = json.totalOlt;
      this.temperatura = json.temperaturaOlt;
      this.voltaje = json.voltajeOlt;
      this.fallaGestion = json.fallaGestionOlt;
      this.down = json.downOlt;
      this.fallaOptica = json.fallaOpticaOlt;
      this.memoria = json.memoriaOlt;
      this.ventilador = json.ventiladorOlt;
      this.disponibilidad = json.disponibilidad;
    } catch (error) {
      
    }



  }
}

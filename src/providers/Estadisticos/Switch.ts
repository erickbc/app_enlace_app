
export class Switch {
    nombre = "SWITCHES";
    id:string = "0";
    down: string = "0";
    temperatura:string = "0";
    fallaOptica:string = "0";
    memoria: string = "0";
    voltaje:string = "0";
    total:string = "0";
    cpu:string = "0";
    ventilador:string = "0";
    fallaGestion:string = "0";
    disponibilidad:string = "0";


    constructor(json) {
      try {
        this.id = json.id;
        this.down = json.downSwitch;
        this.temperatura = json.temperaturaSwitch;
        this.fallaOptica = json.fallaOpticaSwitch;
        this.memoria = json.memoriaSwitch;
        this.voltaje = json.voltajeSwitch;
        this.total = json.totalSwitch;
        this.cpu = json.cpuSwitch;
        this.ventilador = json.ventiladorSwitch;
        this.fallaGestion = json.fallaGestionSwitch;
        this.disponibilidad = json.disponibilidad;
      } catch (error) {
        
      }


    }
}

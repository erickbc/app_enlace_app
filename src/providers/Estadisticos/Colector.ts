
export class Colector {
    nombre = "COLECTORES";
    id:string = "0";
    down: string = "0";
    temperatura:string = "0";
    fallaOptica:string = "0";
    memoria: string = "0";
    voltaje:string = "0";
    total:string = "0";
    cpu:string = "0";
    ventilador:string = "0";
    fallaGestion:string = "0";
    disponibilidad:string = "0";

    constructor(json) {
      try {
        this.temperatura = json.temperaturaColectores;
        this.total = json.totalColectores;
        this.voltaje = json.voltajeColectores;
        this.cpu = json.cpuColectores;
        this.ventilador = json.ventiladorColectores;
        this.fallaGestion = json.fallaGestionColectores;
        this.fallaOptica = json.fallaOpticaColectores;
        this.memoria = json.memoriaColectores;
        this.down = json.downColectores;
        this.disponibilidad = json.disponibilidad;
      } catch (error) {
        
      }


    }
}







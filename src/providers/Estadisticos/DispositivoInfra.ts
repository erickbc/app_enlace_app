import { DetalleInfra } from './DetalleInfra';
export class DispositivoInfra {
  titulo : string;
  expanded= false;
  total:number;
  listaDispositivo =[]


  constructor(json,nombre) {
    this.total=0;
    json.forEach(element => {
      this.listaDispositivo.push(new DetalleInfra(element))
      this.total+=parseInt(element.cantidad)
    });
    this.titulo=nombre;
  }
}

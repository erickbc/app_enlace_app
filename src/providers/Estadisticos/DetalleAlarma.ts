
export class DetalleAlarma {
  fecha : string;
  idAlarma:string;
  falla: string;


  constructor(json) {    
    this.fecha = json.fecha;
    this.idAlarma = json.idAlarma;
    this.falla = json.falla;

  }
}

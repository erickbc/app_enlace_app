
export class Router {
  nombre = "ROUTERS";
  id:string = "0";
  cpu: string = "0";
  memoria:string = "0";
  total:string = "0";
  voltaje: string = "0";
  fallaOptica:string = "0";
  ventilador:string = "0";
  temperatura:string = "0";
  fallaGestion:string = "0";
  down:string = "0";
  disponibilidad:string = "0";


  constructor(json) {
    try {
      this.id = json.id;
      this.cpu = json.cpuRouter;
      this.memoria = json.memoriaRouter;
      this.total = json.totalRouter;
      this.down = json.downRouter;
      this.fallaGestion = json.fallaGestionRouter;
      this.fallaOptica = json.fallaOpticaRouter;
      this.voltaje = json.voltajeRouter;
      this.temperatura = json.temperaturaRouter;
      this.ventilador = json.ventiladorRouter;
      this.disponibilidad = json.disponibilidad;
    } catch (error) {
      
    }

  }

}


import { Injectable } from "@angular/core";
import { Colector } from "./Estadisticos/Colector";
import { Olt } from "./Estadisticos/Olt";
import { Switch } from "./Estadisticos/Switch";
import { Router } from "./Estadisticos/Router";
import { Media } from "./Estadisticos/Media";
import { Baja } from "./Estadisticos/Baja";
import { Critica } from "./Estadisticos/Critica";

@Injectable()
export class Estadisticos {
  public colector:Colector;
  public olt:Olt;
  public router:Router;
  public switch:Switch;
  public media:Media;
  public baja: Baja;
  public critica: Critica;
  constructor() {}
}








import { Injectable } from "@angular/core";
import { Equipo } from "./Equipo";
import { EquipoGeneral } from "./EquipoGeneral";

@Injectable()
export class ListaGenerica {

  public listaGenerica=[];



  setInfraEquipos(json){
    try {
      let tickets = json.listEquipos
      tickets.forEach(ticket => {
          let t = new Equipo(ticket);
          this.listaGenerica.push(t);
      });
       
     } catch (error) {
 
     }
  }

  setInfraAlarmas(json){
    try {
      let tickets = json.listAlarmas
      tickets.forEach(ticket => {
          let t = new Equipo(ticket);
          t.alarma = true
          this.listaGenerica.push(t);
      });
       
     } catch (error) {
 
     }
  }

  setInfraTotales(json){
    try {
      let tickets = json.listEquipos
      tickets.forEach(ticket => {
          let t = new EquipoGeneral(ticket);
          this.listaGenerica.push(t);
      });
       
     } catch (error) {
 
     }
  }








}
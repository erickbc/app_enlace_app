
import { Injectable } from "@angular/core";

@Injectable()
export class Equipo {

    public ip="";
    public alarma=false
    public nombre=""
    public tituloAlarma=""
    public fechaAlarma=""
    public nivel=""
    public expanded = false
    public ticket=""
    public color=""
  
 

    constructor(json) {

      try {
        if(json.ip==null){
          this.ip = "Sin IP";
        }else if (json.ip == ""){
          this.ip = "Sin IP";
        }else{
          this.ip = json.ip;
        }
        
       


        this.alarma = json.alarma
        this.nombre = json.modelName
        
        if(json.tituloAlarma) {
          this.tituloAlarma = json.tituloAlarma
        }
        if(json.fechaAlarma) {
          this.fechaAlarma = json.fechaAlarma
        }
        if(json.severity) {
          this.nivel = json.severity
        }  
        if(json.numeroTicket == null) {
          this.ticket =  "Sin Ticket";
        } else if(json.numeroTicket == "") {
          this.ticket = "Sin Ticket";
        }else{
          this.ticket = json.numeroTicket;
        }  
        if(json.color) {
          this.color = json.color
        }  
       
      } catch (error) {
  
      }
  
    }





}
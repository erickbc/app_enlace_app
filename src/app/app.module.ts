import { ViewMonitoreo } from './../pages/view-monitoreo/view-monitoreo';
import { ViewListaAlarmas } from './../pages/view-listaAlarmas/view-listaAlarmas';
import { ViewAvisoLegalUser } from './../pages/view-aviso-legalUser/view-aviso-legalUser';
import { ViewEditarIncidente } from './../pages/view-editarIncidente/view-editarIncidente';
import { ViewAltaUsuario } from './../pages/view-altaUsuario/view-altaUsuario';
import { ViewBusquedaIncidente } from './../pages/view-busquedaIncidentes/view-busquedaIncidentes';
import { ViewEditarUsuario } from './../pages/view-editarUsuario/view-editarUsuario';



import { ViewAcceso } from './../pages/view-acceso/view-acceso';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ViewInformes } from '../pages/view-informes/view-informes';
import { ViewIncidente } from '../pages/view-incidentes/view-incidentes';
import { ViewDetalleInformes } from '../pages/view-detalleInformes/view-detalleInformes';
import { ViewUsuarios } from '../pages/view-usuarios/view-usuarios';
import { ViewAltaIncidente } from '../pages/view-altaIncidente/view-altaIncidente';
import { ViewLogotipo } from '../pages/view-logotipo/view-logotipo';
import { Camera } from '@ionic-native/camera';
import { ViewAvisoLegal } from '../pages/view-aviso-legal/view-aviso-legal';
import { GestionWebServices } from '../providers/GestionWebServices';
import { ViewResume } from '../pages/view-resume/view-resume';
import { Estadisticos } from '../providers/Estadisticos';
import { ViewGraficaGenerica } from '../pages/view-grafica-generica/view-grafica-generica';
import { ViewTickets } from '../pages/view-tickets/view-tickets';
import { ViewNivelesGenerales } from '../pages/view-NivelesGenerales/view-nivelesGenerales';
import { ViewNivelesDetalle } from '../pages/view-NivelesDetalle/view-nivelesDetalle';
import { Parameters } from '../providers/Parameters';
import { ViewListaInfraestructura } from '../pages/view-listaInfraestructura/view-listaInfraestrctura';
import { ViewResumeInfra } from '../pages/view-resumeInfra/view-resumeInfra';
import { TabsInfraPage } from '../pages/tabsInfra/tabsInfra';
import { ExpandableComponent } from '../components/expandable/expandable';
import { ListaInfra } from '../providers/Estadisticos/ListaInfra';
import { FCM } from '@ionic-native/fcm';
import { HeaderColor } from '@ionic-native/header-color';
import { ViewListaTotalInfraestructura } from '../pages/view-listaTotalInfraestructura/view-listaTotalInfraestrctura';
import { ViewDetalleEspecifico } from '../pages/view-detalleEspecificoNoCare/view-detalleEspecificoNoCare';
import { Keychain } from '@ionic-native/keychain';
import { CommonVariablesProvider } from '../providers/Utils/CommonVariablesProvider';
import { CommonUtilsProvide } from '../providers/Utils/CommonUtilsProvide';
import { SessionVariables } from '../providers/Session/SessionVariables';

@NgModule({
  declarations: [
    MyApp,
    ViewDetalleInformes,
    ViewIncidente,
    ViewInformes,
    TabsPage,
    TabsInfraPage,
    ViewAcceso,
    ViewUsuarios,
    ViewEditarUsuario,
    ViewBusquedaIncidente,
    ViewAltaUsuario,
    ViewEditarIncidente,
    ViewAltaIncidente,
    ViewLogotipo,
    ViewAvisoLegal,
    ViewAvisoLegalUser,
    ViewListaAlarmas,
    ViewTickets,
    ViewResume,
    ViewResumeInfra,
    ViewGraficaGenerica,
    ViewNivelesGenerales,
    ViewNivelesDetalle,
    ViewMonitoreo,
    ViewListaInfraestructura,
    ExpandableComponent,
    ViewListaTotalInfraestructura,
    ViewDetalleEspecifico

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ViewDetalleInformes,
    ViewIncidente,
    ViewInformes,
    ViewAvisoLegalUser,
    TabsPage,
    TabsInfraPage,
    ViewAcceso,
    ViewUsuarios,
    ViewEditarUsuario,
    ViewBusquedaIncidente,
    ViewAltaUsuario,
    ViewEditarIncidente,
    ViewAltaIncidente,
    ViewLogotipo,
    ViewAvisoLegal,
    ViewResume,
    ViewResumeInfra,
    ViewGraficaGenerica,
    ViewListaAlarmas,
    ViewTickets,
    ViewNivelesGenerales,
    ViewNivelesDetalle,
    ViewMonitoreo,
    ViewListaInfraestructura,
    ViewListaTotalInfraestructura,
    ViewDetalleEspecifico
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Storage,
    GestionWebServices,
    Estadisticos,
    Parameters,
    ListaInfra,
    FCM,
    HeaderColor,
    Keychain,
    CommonUtilsProvide,
    CommonVariablesProvider,
    SessionVariables
  ]
})
export class AppModule {}

import { FCM } from '@ionic-native/fcm';
import { ViewAcceso } from './../pages/view-acceso/view-acceso';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, App, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HeaderColor } from '@ionic-native/header-color';
import { ViewUsuarios } from '../pages/view-usuarios/view-usuarios';
import { ViewLogotipo } from '../pages/view-logotipo/view-logotipo';
import { ViewAvisoLegal } from '../pages/view-aviso-legal/view-aviso-legal';
import { Events } from 'ionic-angular/util/events';
import { GestionWebServices } from '../providers/GestionWebServices';
//import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = ViewAcceso;
  @ViewChild("contenido") menu:NavController;
  ams = ViewUsuarios
  log = ViewLogotipo
  avs = ViewAvisoLegal
  typeUser = ""
  splashScreen:any;
  tokenDevice = ""
  constructor(platform: Platform,
              public statusBar: StatusBar,
              splashScreen: SplashScreen,
              public events:Events,
              public menuCtrl: MenuController,
              public app: App,
              private fcm: FCM,
              private webS: GestionWebServices,
              private headerColor: HeaderColor
              ) {
    console.log("CONTRUCTOR MYAPP")




    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
        /*
        //==================Notifications==================
        fcm.subscribeToTopic('all');
        fcm.getToken().then(token=>{
            console.log("=====IFNROMATION ABOUT TOKEN=====");
            console.log(token);
            this.tokenDevice = token
            console.log("=====IFNROMATION ABOUT TOKEN=====");


            events.subscribe('user:token', (username) => {
              //TOODO
              console.log("====EMIT SUSBCRBIBE USER NAME ===")
              console.log(username)

              console.log("====EMIT SUSBCRBIBE USER TOKEN ===")
              let information = {username: username, tokenId: this.tokenDevice}
              console.log("====INFORMATION ABOUT CALL SERVICE===")
              console.log(information)
        
              this.webS.setTokenDevice(information).subscribe(data => {
                console.log("====RESPONSE SERVICE===")
                console.log(data)
              });
            });


        })
        fcm.onNotification().subscribe(data=>{
          if(data.wasTapped){
            console.log("Received in background");
          } else {
            console.log("Received in foreground");
          };
        })
        fcm.onTokenRefresh().subscribe(token=>{
          console.log(token);
        });
        //==================Notifications==================
        */
        
       this.hideSplashScreen();
      statusBar.styleDefault();
     
      

        events.subscribe('statusBar:changeColor', () => {
          console.log('APP Component - Change Color')          
          //=========CHANGE==========
          this.statusBar.overlaysWebView(false);
          //headerColor.tint('#000000');
          this.statusBar.styleLightContent();

          this.statusBar.backgroundColorByHexString("#232323");
          //=========CHANGE==========
        });
    
      splashScreen.hide();
    });

  
    events.subscribe('user:login', (typeU) => {
      this.typeUser = typeU
    });




  }



  hideSplashScreen() {
    if (this.splashScreen) {
    setTimeout(() => {
      this.splashScreen.hide();

    }, 100);
    }
  }

  irAPagina(pagina:any){
    console.log("Ir a Pagina")
    //this.menu.setRoot(pagina);
    this.menu.push(pagina);

    //this.menuCtrl.close
  }

  cerrarSesion(){
    this.app.getRootNavs()[0].push(ViewAcceso);
    /*
    this.storage.set("tokeUser", "");
    this.storage.set("idUser", "");
    this.storage.set("tipoUsuario", "");
    this.storage.set("alarmas", "");

    */

  }

}

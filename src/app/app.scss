// http://ionicframework.com/docs/theming/


// App Global Sass
// --------------------------------------------------
// Put style rules here that you want to apply globally. These
// styles are for the entire app and not just one component.
// Additionally, this file can be also used as an entry point
// to import other Sass files to be included in the output CSS.
//
// Shared Sass variables, which can be used to adjust Ionic's
// default Sass variables, belong in "theme/variables.scss".
//
// To declare rules for a specific mode, create a child rule
// for the .md, .ios, or .wp mode classes. The mode class is
// automatically applied to the <body> element in the app.
.profile {
    top: 0;
    right: 0;
    left: 0;
    z-index: 0;
    overflow: hidden;
    text-align: center;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 100%;
    padding-top: 15px;
}

@font-face {
  font-family: 'fontAlarmasTP';
  src:  url('../assets/fonts/fontAlarmasTP.eot?yjxfh8');
  src:  url('../assets/fonts/fontAlarmasTP.eot?yjxfh8#iefix') format('embedded-opentype'),
    url('../assets/fonts/fontAlarmasTP.ttf?yjxfh8') format('truetype'),
    url('../assets/fonts/fontAlarmasTP.woff?yjxfh8') format('woff'),
    url('../assets/fonts/fontAlarmasTP.svg?yjxfh8#fontAlarmasTP') format('svg');
  font-weight: normal;
  font-style: normal;
}

i {
  /* use !important to prevent issues with browser extensions that change fonts */
  font-family: 'fontAlarmasTP' !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;

  /* Better Font Rendering =========== */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.icon-monitoreo:before {
  content: "\e900";
}
.icon-tickets:before {
  content: "\e901";
}
.icon-user:before {
  content: "\e902";
}
.icon-password:before {
  content: "\e903";
}
.icon-salud:before {
  content: "\e904";
}
.icon-performance:before {
  content: "\e905";
}
.icon-infraestructura:before {
  content: "\e906";
}
.icon-resumen:before {
  content: "\e907";
}
.icon-routers:before {
  content: "\e908";
}
.icon-switches:before {
  content: "\e909";
}
.icon-olts:before {
  content: "\e90a";
}
.icon-colectores:before {
  content: "\e90b";
}
.icon-alarma:before {
  content: "\e90c";
}
.icon-cerrar:before {
  content: "\e90d";
}
.icon-CARE:before {
  content: "\e90e";
}
.icon-NOC:before {
  content: "\e90f";
}


/* Estilos Generales */


.contieneLogo{
  background-color:rgba(0,0,0,0) !important;
  border-bottom:none !important;

}

.poweredText{
  margin:0 auto;
  padding-bottom:10px;
  color:#757575;
}

.tieneLogo{
  padding-top: 10px;
}

.logoTotal{
  background-color:rgba(0,0,0,0);
}

.contenedorGrid{
  margin-left:-16px;
  margin-right:-16px;
}

.contenedorGrid2{
  margin-left:-16px;
}

.listaCard{
  margin-top:-10px;
}

.contenedorAppModule{
  margin-left:-16px;
  margin-right:-32px;
}

.contenedorAppModule2{
   margin-left:-16px;
   margin-right:-16px;
}

.modulo{
  background-color:rgba(0,0,0,0);
  padding-left:0px;
  margin-bottom:-18px;
}

.modulo2{
  background-color:rgba(0,0,0,0);
  margin-bottom:-18px;
}

.claseDevice{
  text-align:left;
  font-size:1em !important;
  font-weight:bold !important;
}

.subEquipos{
  font-size:0.8em !important;
  text-align:right;
}

.totalEquipos{
  font-size:1.2em !important;
  font-weight:bold;
}

.labelAlarma{
  font-size:0.9em !important;
  font-weight:bold;
}

.dato{
    font-weight: bold;
}

.reductor{
  padding:0px !important;
  margin-top:-5px;
  margin-bottom:-7px;
}

.botonSinBorde{
  border:none !important;
  margin-bottom:-28px !important;
}

.panelAcordeon{
  padding-left:0px;

}

.tituloAcordeon{
  text-align:left !important;
  font-size:1.1em !important;
  font-weight:600 !important;
  letter-spacing:.1em !important;
}

.indicadorLateralOK{
  color:#6fe805;
}

.indicadorLateralAlarma{
  color:#ff0000;
}

.indicadorText{
  font-size:1.1em !important;
  font-weight:600 !important;
  text-align:right;
}

.expandible{
  text-align:left;
  margin-top:75px !important;
}

.expandible p{
  font-size:11pt !important;
  letter-spacing:2pt !important;
  font-weight:600 !important;
  padding-left:5px !important;
}

.greyLabel{
  font-size:11pt !important;
  color:#ddd !important;
}

.nombreDisp{
  font-size:.8em !important;
}

.cantidadDisp{
  font-size:.9em !important;
}

.nombreOK{
  color:#aaaaaa !important;
}

.cantidadOK{
  color:#6fe805 !important;
}

.nombreAlarma{
  color:#ffffff !important;
}

.cantidadAlarma{
  color:#ff0000 !important;
  font-weight:600 !important;
}


.fondoClaro{
    background: linear-gradient(45deg, rgba(228,228,228,1) 0%, rgba(255,255,255,1) 50%);
}

.fondoOscuro{
    background: linear-gradient(to bottom, rgba(10,10,10,1) 0%, rgba(50,50,50,1) 50%, rgba(10,10,10,1) 100%);
}


path{
  stroke:rgba(0,0,0,0) !important;
}

.header-md::after, .tabs-md[tabsPlacement="top"] > .tabbar::after, .footer-md::before, .tabs-md[tabsPlacement="bottom"] > .tabbar::before {
    background-image: none !important;
}

.toolbar-title-md {
    color: #fff !important;
}

.tituloPagina{
  color:#ffffff !important;
  font-size:.9em;
  font-weight:500 !important;
  letter-spacing:.13em;
}

.botonSeccion{
  background-color:rgba(0,0,0,0) !important;
  box-shadow:none !important;
  text-transform:none !important;
  font-size:15pt !important;
  font-weight:400;
  letter-spacing:2pt;
}

.botonSeccion:after{
  content: "";
    width: 100%;
    height: 3px;
    background-color: #b81532;
    background: linear-gradient(to right, rgba(0,0,0,0) 30%,#b81532 50%,rgba(0,0,0,0) 70%);
    left: 0;
    position: absolute;
    display: block;
    top: 43px;
}

.botonSeccionDes{
  color: #838383 !important;
}

.botonSeccionDes:after{
  background-color: rgba(0,0,0,0);
  background: linear-gradient(to right, rgba(0,0,0,0) 30%,rgba(0,0,0,0) 50%,rgba(0,0,0,0) 70%);
}


.selectorMonitoreo{
  margin-top:40px;
}


.blackCard{
    background: linear-gradient(to bottom, #212121 0%,#1d1d1d 100%);
    color:#ffffff  !important;
    text-align:center !important;
    font-family: "HelveticaNeue-Light", "Helvetica Neue", Helvetica, Arial, sans-serif;
    padding-bottom:0px;
}


.blackCardWithBorder{
    background-color: rgba(46,46,46,0.45);
    border-radius: 10px;
    color:#ffffff  !important;
    text-align:center !important;
    border:1px solid #4a4a4a;
    font-family: "HelveticaNeue-Light", "Helvetica Neue", Helvetica, Arial, sans-serif;
    padding:10px;
  }

.textoCard{
    font-family: "HelveticaNeue-Light", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}

.blackCard2{
    width: 46%;
    margin: 0px 2%;
    background-color: rgba(28,28,28,0.5);
    border-radius: 5px;
    color:#ffffff  !important;
    text-align:center !important;
    font-family: "HelveticaNeue-Light", "Helvetica Neue", Helvetica, Arial, sans-serif;
}

.graficaTerca{
    height: 100px !important;
}

.tituloLight{
    margin-top: 20px;
}

.tituloLista{
  margin-top:25px;
  text-align:center;
  font-size:1.5em !important;
}

.botonCerrar{
    background-color: rgba(0,0,0,0);
    border: 1px solid #ff0000;
    color: #ffffff;
    float:right;
}

.botonCerrar:hover{
    background-color: rgba(0,0,0,0) !important;
    border: 1px solid rgba(208, 2, 27, 1) !important;
    color: rgba(208, 2, 27, 1) !important;
}

.dato{
    font-weight: bold;
    font-size: 1.5em !important;
}

.panelTituloInferior{
  background-color:#000;
  background: linear-gradient(to bottom, #2c2c2c 0%,#010101 100%);
  margin-left:10px;
  margin-right:10px;
  height:30px;
}

.panelTituloInferiorNoMargen{
  background-color:#000;
  background: linear-gradient(to bottom, #2c2c2c 0%,#010101 100%);
  height:30px;
}

.regreso{
  color:#fff;
  font-size: 2.5em;
  background-color:rgba(0,0,0,0) !important;
  padding:15px;
}

.inactivo{
  color:#555 !important;
}

.noPaddingVertical{
  padding:0px !important;
}

.blueTitle{
  background: linear-gradient(to bottom, #57C5E5 0%,#134E6B 100%);
  padding-top:0px !important;
}

.resultadoTabla{
  background: linear-gradient(to bottom, #141414 0%,#111111 100%);
  padding-top:0px !important;
  margin-top:3px !important;
  margin-left:-6px;
  padding-left:10px;
}

.blueTotal{
  background-color:rgba(0,0,0,0.55);
  padding-top:10px !important;
  padding-bottom:10px !important;
  text-align:center !important;
}

.blueAfectados{
  background-color:rgba(0,0,0,0.65);
  padding-top:10px !important;
  padding-bottom:10px !important;
  text-align:center !important;
  color:#FF001F !important;
}

.listaDispositivos{
  text-align:left !important;
  font-size:12pt !important;
  font-weight:600;
}

.grisTotal{
  background-color:rgba(255,255,255,.15);
  padding-top:10px !important;
  padding-bottom:10px !important;
  text-align:center !important;
  font-size:18pt !important;
  font-weight:600 !important;
}

.grisAfectados{
  background-color:rgba(255,255,255,.1);
  padding-top:10px !important;
  padding-bottom:10px !important;
  text-align:center !important;
  font-size:18pt !important;
  font-weight:600 !important;
}

.tablaTitulo{
  font-size:12pt !important;
  font-weight:600!important;
}

.barraBusqueda{
  
  background-color:rgba(0,0,0,0) !important;
}

.barraBusqueda .searchbar-input {
    border:2px solid #57C5E5 !important;
    padding: 6px 30px !important;
    border-radius: 25px;
    background-position: left 8px center;
    height: auto;
    font-size: 14pt;
    font-weight: 600;
    line-height: 3rem;
    color: #57C5E5 !important;
    background-color:rgba(0,0,0,0) !important;
    -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}

.barraBusqueda .searchbar-input::placeholder{
  color:#57C5E5 !important;
}

.barraBusqueda .searchbar-search-icon {
    background-image:none !important;
}

.alarmas_list{
  margin-left:-16px !important;
  margin-right:-16px !important;
  padding:0px !important;
}

.colocadorInfraestructura{
  padding-left:5px !important;
  padding-right:0px !important;
  position:relative !important;
  min-height:90px !important;
}

.textoBlanco{
  color:#ffffff !important;
}

.textoAzul{
  color:#57C5E5 !important;
}

.textoGris{
  color:#757575 !important;
  font-size:1.1em !important;
  font-weight:600 !important;
  vertical-align: top !important;
}

.textoGrisWrap{
  color:#757575 !important;
  font-size:1.1em !important;
  font-weight:600 !important;
}

.contieneAlarma{
  position:absolute;
  left:0px !important;
  top:0px !important;
  right:0px !important;
  bottom:0px !important;
  
}

.panelAcordeonRojo{
  padding:0px;
  background: linear-gradient(to bottom, #ff0000 0%, #4E0000 100%);
}

.panelInfo{
  padding:10px 0px 10px 20px !important;
}

.botonRegistrarTicket{
  border:2px solid #ff0000;
  background-color: rgba(0,0,0,0);
  color:#757575;
  font-weight:600;
}

.darkCard{
  background-color:#333333;
  margin-top:3px;
  margin-bottom:-25px;
  padding:3px !important;
  font-size:12pt !important;
}

.ticketLabel{
  margin:0 auto !important;
}

.ticketDato{
  margin:0 auto !important;
  color:#FF9E00;
}

.abiertoDato{
  margin:0 auto !important;
  color:#60A9FF;
}

.categoriaDato{
  color:#50E3C2;
}

.fechaAcordeon{  
    width: calc(100% + 20px);
    margin: 0px;
    padding: 5px 10px;
    text-align: center;
    font-size: 14pt;
    color: #bbb;
}

.fechaAcordeon h2{
  font-size:13pt !important;
  letter-spacing:1.5pt !important;
  font-weight:600 !important;
}

.fechaAcordeon h3{
  font-size:12pt !important;
  letter-spacing:1.5pt !important;
  font-weight:400 !important;
}

.contenedorTarjetasOscuras{
  margin-left:-16px !important;
  margin-right:-16px !important;
}

.contenedorSliding{
  background-color:rgba(0,0,0,0) !important;
}

.tarjetaOscura{
  background: linear-gradient(to bottom, rgba(30,30,30,1) 0%, rgba(10,10,10,1) 100%);
  margin-bottom:2px !important;
  color:#ffffff !important;
  height:70px !important;
  border-bottom:none !important;
}

.estatusActivacion{
  display:flex !important;
  justify-content:center !important;
  align-items:center !important;
  justify-items:center !important;
}

.etiquetaActivacion{
  width:60% !important;
  margin-left:10px !important;
  font-size:10pt !important;
  color:#aaaaaa !important;
}

.usuarioInactivo{
  background-color:#ff0000 !important;
  width:7px !important;
  height:7px !important;
  border-radius:50% !important;
  box-shadow:0px 0px 15px 6px rgba(255,0,0,.5) !important;
}

.usuarioActivo{
  background-color:#83ff00 !important;
  width:7px !important;
  height:7px !important;
  border-radius:50% !important;
  box-shadow:0px 0px 15px 6px rgba(131,255,0,.5) !important;
}

.usernameLabel{
  color:#bbbbbb !important;
  font-size:18pt !important;
}

.nameLabel{
  font-size:13pt !important;
}

.opcionesUsuario{
  height:70px !important;
}

.botonEditar{
  background: linear-gradient(to bottom, rgba(30,30,30,1) 0%, rgba(10,10,10,1) 100%);
  color:#57C5E5 !important;
}

.botonEditar:after{
  display: block;
  content:"";
  position: absolute;
  bottom: 4px;
  left: 20%;
  background-color:#57C5E5 !important;
  width:60% !important;
  height:2px !important;
  border-radius:3px !important;
  box-shadow:0px 0px 15px 6px rgba(87,197,229,.5) !important;
}

.botonEliminar{
  background: linear-gradient(to bottom, rgba(30,30,30,1) 0%, rgba(10,10,10,1) 100%);
  color:#ff0000 !important;
}

.botonEliminar:after{
  display: block;
  content:"";
  position: absolute;
  bottom: 4px;
  left: 20%;
  background-color:#ff0000 !important;
  width:60% !important;
  height:2px !important;
  border-radius:3px !important;
  box-shadow:0px 0px 15px 6px rgba(255,0,0,.5) !important;
}

.tituloPanelUsuario{
  color:#ffffff !important;
  text-align: center !important;
  font-size:16pt !important;
  font-weight:600 !important;
  margin-bottom:30px !important;
}

.campoUsuario{
  background-color:rgba(0,0,0,0) !important;
  // border-bottom: 1px solid #57C5E5 !important;
  margin-bottom:20px !important;
}

.etiquetaPanelUsuario{
  color:#57C5E5 !important;
  font-size:16pt !important;
}

.noEdicion{
  color:#888888 !important;
}

.editaUsuario{
  color:#ffffff !important;
  border-bottom: 1px solid #57C5E5 !important;
}

.botonGenerar{
  border: 1px solid #57C5E5 !important;
  background-color: rgba(0,0,0,0) !important;
  border-radius:25px !important;
}

.botonActualizar{
  border: 1px solid #ff0000 !important;
  background-color: rgba(0,0,0,0) !important;
  border-radius:25px !important;
}

.botonCancelar{
  border: 1px solid #57C5E5 !important;
  background-color: rgba(0,0,0,0) !important;
  border-radius:25px !important;
}



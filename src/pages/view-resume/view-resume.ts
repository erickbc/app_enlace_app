import { Parameters } from './../../providers/Parameters';
import { Component } from '@angular/core';
import { Tabs, NavController, NavParams, LoadingController, Loading, AlertController, App } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { Estadisticos } from '../../providers/Estadisticos';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';
import { ViewListaAlarmas } from '../view-listaAlarmas/view-listaAlarmas';

/**
 * Generated class for the ViewAvisoLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'view-resume',
  templateUrl: 'view-resume.html',
})
export class ViewResume{

  loading: Loading;


  items = [];

  tab:Tabs;

  timer=0
  subscriptionFirst: any;
  tickFirst: any;

  constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private loadingCtrl: LoadingController,
        public webService: GestionWebServices,
        private alertCtrl: AlertController,
        public estadisticos:Estadisticos,
        public app: App,
        private parameters :Parameters
      ) {
        console.log('constructor ViewResume');
        this.tab = this.navCtrl.parent;
        this.timer = this.parameters.timer;

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewResume');
    this.loadEstadisticas(true)
  }


  cargando() {
    console.log('cargando-ViewResume');
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
      
  retorno(){
    this.navCtrl.setRoot(ViewMonitoreo);
  }


  mostrarError(data) {
    console.log('mostrarError-ViewResume');
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error '+data.status,
      subTitle: data.statusText,
      buttons: ['OK']
    });
    alert.present()
  }

  abrirListaAlarmas(parametro){
    console.log("abrirListaAlarmas - ViewResume");
    this.navCtrl.push(ViewListaAlarmas,{parametro : parametro});
  }


  selectItem(item,option){

    console.log('selectItem-ViewResume');


    switch (item.nombre) {
      case "COLECTORES":

      this.navCtrl.push(ViewListaAlarmas,{parametro : "3",alarma:option});
        break;
      case "OLTS":
      this.navCtrl.push(ViewListaAlarmas,{parametro : "4",alarma:option});
        break;
      case "ROUTERS":
        this.navCtrl.push(ViewListaAlarmas,{parametro : "1",alarma:option});
        break;
      case "SWITCHES":
      this.navCtrl.push(ViewListaAlarmas,{parametro : "2",alarma:option});
        break;

    }



  }

   //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log('ionViewWillLeave-ViewResume');
      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log('ionViewWillEnter-ViewResume');
      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        this.loadEstadisticas(false)
      });
    }


  public loadEstadisticas(online) {
    console.log('loadEstadisticas-ViewResume');
    this.webService.getEstadisticas().subscribe(data => {
      if(data.status==200){
        this.items = [];
        this.items.push(data.estadisticos.router);
        this.items.push(data.estadisticos.switch);
        this.items.push(data.estadisticos.colector);
        this.items.push(data.estadisticos.olt);
      }
      else {
        if(online){
          this.mostrarError(data);
          this.ionViewWillLeave();
          this.saliendo();
        }
      }
    },
    error => {
      if(online){
        this.mostrarError(error);
        this.ionViewWillLeave();
        this.saliendo()
      }
    });
  }

  saliendo(){
    console.log('saliendo-ViewResume');
    this.app.getRootNav().setRoot(ViewMonitoreo)
    //this.navCtrl.setRoot(ViewMonitoreo)
  }


}

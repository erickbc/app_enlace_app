import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { ViewNivelesDetalle } from '../view-NivelesDetalle/view-nivelesDetalle';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Parameters } from '../../providers/Parameters';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';

/**
 * Generated class for the ViewAvisoLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'view-nivelesGenerales',
  templateUrl: 'view-nivelesGenerales.html',
})
export class ViewNivelesGenerales{

  loading: Loading;

  arrayInformation = []

  subscriptionFirst: any;
  tickFirst: any;
  timer = 0

  tipo:String;
  constructor(
        public navCtrl: NavController,
        private parameters : Parameters,
        public navParams: NavParams,
        private loadingCtrl: LoadingController,
        public webService: GestionWebServices,
        private alertCtrl: AlertController,
        public  params:NavParams,

      ) {
        console.log('constructor ViewNivelesGenerales');

     this.timer = this.parameters.timer
     this.tipo = params.data

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewNivelesGenerales');
    if (this.tipo == "NOC"){
      this.loadingInformationNOC(true)
    }else{
      this.loadingInformationCare(true)
    }

  }

  retorno(){
    this.navCtrl.setRoot(ViewMonitoreo);
  }
  
  cargando() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  mostrarError() {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'No es posible mostrar la información en estos momentos.',
      buttons: ['OK']
    });
    alert.present()
  }

  loadingInformationNOC(online){
    console.log('loadingInformationNOC ViewNivelesGenerales');
    if(online){
      this.cargando();
    }
    this.webService.getNocGeneral().subscribe(data => {
      if (data.status==200) {
        this.arrayInformation = []
        this.arrayInformation.push(data.nocGeneral.frontOffice)
        this.arrayInformation.push(data.nocGeneral.backOffice)
        if(online){
          this.loading.dismiss();
        }
      }
      else {
        if(online){
          this.mostrarError()
          this.navCtrl.pop()
        }
      }
    },
    error => {
      if(online){
        this.mostrarError()
        this.navCtrl.pop()
      }
    });
  }
  //Cada vez que sale de la pantalla se muere el timer que mantiene
  //actualizado la pantalla
  ionViewWillLeave() {
    this.subscriptionFirst.unsubscribe();
  }
  //Cada vez que entra a la pantalla se inicia el timer que mantiene
  //actualizado la pantalla
  ionViewWillEnter(){
    let firstTimer = TimerObservable.create(this.timer, this.timer);
    this.subscriptionFirst = firstTimer.subscribe(t => {
      this.tickFirst = t;
      if (this.tipo == "NOC"){
        this.loadingInformationNOC(false)
      }else{
        this.loadingInformationCare(false)
      }

    });
  }
  loadingInformationCare(online){
    if(online){
      this.cargando();
    }
    
    this.webService.getCareGeneral().subscribe(data => {
      if (data.status==200) {
        this.arrayInformation = []
        this.arrayInformation.push(data.careGeneral.monitoreo)
        this.arrayInformation.push(data.careGeneral.estrategico)
        this.arrayInformation.push(data.careGeneral.gobierno)
        if(online){
          this.loading.dismiss();
        }
      }
      else {
        if(online){
          this.mostrarError();
          this.navCtrl.pop()
        }
      }

    },
    error => {
      if(online){
        this.mostrarError();
        this.navCtrl.pop()
      }
    });
  }

  seleccion(nombre){
    let d = {nombre:nombre,tipo:this.tipo}
    this.navCtrl.push(ViewNivelesDetalle,d);


  }

}

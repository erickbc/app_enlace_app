import { Parameters } from './../../providers/Parameters';
import { Component } from '@angular/core';
import { Tabs, NavController, NavParams, LoadingController, Loading, AlertController, App } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { Estadisticos } from '../../providers/Estadisticos';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ViewListaTotalInfraestructura } from '../view-listaTotalInfraestructura/view-listaTotalInfraestrctura';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';

/**
 * Generated class for the ViewAvisoLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'view-resumeInfra',
  templateUrl: 'view-resumeInfra.html',
})
export class ViewResumeInfra{

  loading: Loading;


  items = [];

  tab:Tabs;

  timer=0
  subscriptionFirst: any;
  tickFirst: any;



  constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private loadingCtrl: LoadingController,
        public webService: GestionWebServices,
        private alertCtrl: AlertController,
        public estadisticos:Estadisticos,
        public app: App,
        private parameters :Parameters,
        public nav: NavController
      ) {
        console.log("constructor - ViewResumeInfra")
        this.tab = this.navCtrl.parent;
        this.timer = this.parameters.timer;
        
  }


 
    
  retorno(){
    this.navCtrl.setRoot(ViewMonitoreo);
  }


  seleccionInfra(item,tipo){
    console.log('seleccionInfra ViewResume');
    let d = {item:item,tipo:tipo}
    //1.- Total 2.-Errores
    this.nav.push(ViewListaTotalInfraestructura,d);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewResume');
    this.loadEstadisticas(true)
  }


  cargando() {
    console.log("cargando - ViewResumeInfra")
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  mostrarError(data) {
    this.loading.dismiss();
    console.log("mostrarError - ViewResumeInfra")
    let alert = this.alertCtrl.create({
      title: 'Error '+data.status,
      subTitle: data.statusText,
      buttons: ['OK']
    });
    alert.present()
  }

  selectItem(item){
    console.log("selectItem - ViewResumeInfra")
    switch (item.nombre) {
      case "MEDIA":
        this.tab.select(2);
        break;
      case "BAJA":
        this.tab.select(3);
        break;
      case "CRITICA":
        this.tab.select(1);
        break;
    }
  }

   //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log("ionViewWillLeave - ViewResumeInfra")
      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log("ionViewWillEnter - ViewResumeInfra")
      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        this.loadEstadisticas(false)
      });
    }


  public loadEstadisticas(online) {
    console.log("loadEstadisticas - ViewResumeInfra")
      if(online){
      this.cargando();
    }

    this.webService.getEstadisticasInfra().subscribe(data => {
      if(data.status==200){
        this.items = data.estadisticos.listaGenerica;
        if(online){
          this.loading.dismiss();
        }
      }
      else {
        if(online){
          this.mostrarError(data);
          this.ionViewWillLeave();
          this.saliendo()
        }
      }
    },
    error => {
      if(online){
        this.mostrarError(error);
        this.ionViewWillLeave();
        this.saliendo()
      }
    });
  }

  saliendo(){
    console.log("saliendo - ViewResumeInfra")
    this.app.getRootNav().pop()
  }


}

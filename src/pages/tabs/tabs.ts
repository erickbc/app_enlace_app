import { NavParams, ModalController } from 'ionic-angular';



import { Component } from '@angular/core';

import { ViewAvisoLegalUser } from '../view-aviso-legalUser/view-aviso-legalUser';
import { ViewResume } from '../view-resume/view-resume';
import { ViewGraficaGenerica } from '../view-grafica-generica/view-grafica-generica';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
/*
  tabInformes = ViewInformes;
  tabIncidentes = ViewIncidente;
  tabDetalleinformes = ViewDetalleInformes;
*/
tabResume =  ViewResume;
tabAviso = ViewAvisoLegalUser;

tabGeneric = ViewGraficaGenerica;
alarmas = false

typeTabRouter = "1";
typeTabSwitch = "2";
typeTabColector = "3";
typeTabOTL = "4";

  constructor(public params:NavParams,
    public modalCtrl: ModalController) {
    this.alarmas = params.data.alarmas

  }


}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the ViewLogotipoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-logotipo',
  templateUrl: 'view-logotipo.html',
})
export class ViewLogotipo {
  base64Image:any;
  constructor(public camera:Camera,public navCtrl: NavController, public navParams: NavParams) {
  }

  accesoGaleria(){
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL
     }).then((imageData) => {
       this.base64Image = 'data:image/jpeg;base64,'+imageData;
      }, (err) => {
       console.log(err);
     });
   }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewLogotipoPage');
  }

}

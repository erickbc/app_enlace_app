import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewLogotipo } from './view-logotipo';

@NgModule({
  declarations: [
    ViewLogotipo,
  ],
  imports: [
    IonicPageModule.forChild(ViewLogotipo),
  ],
})
export class ViewLogotipoPageModule {}

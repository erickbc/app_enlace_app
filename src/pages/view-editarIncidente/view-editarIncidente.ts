import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'view-editarIncidente.html'
})
export class ViewEditarIncidente {

  listaUsuarios= []
  nombreUsuario = "El Julion"
  grupoUsuario = "Cumbion"
  contraseniaUsuario = "12345"
  numeroLogin = "12345"

  tabBarElement: any;


  constructor(private alertCtrl: AlertController,public navCtrl: NavController) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

  }


  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }


cancelar(){
  this.navCtrl.pop()
}

seleccionEstatus(){
  let prompt = this.alertCtrl.create({
    title: 'Cambiar Estatus',
    message: 'Selecciona el Estatus: ',
    inputs : [
    {
        type:'radio',
        label:'En Curso',
        value:'Curso'
    },
    {
      type:'radio',
      label:'Cerrado',
      value:'Cerrado'
  },
  {
    type:'radio',
    label:'Solucionado',
    value:'Solucionado'
},
  ],
    buttons : [
    {
        text: "Cancelar",
        handler: data => {
        }
    },
    {
        text: "Seleccionar",
        handler: data => {
        }
    }]});
    prompt.present();
}

  alta() {
    let alert = this.alertCtrl.create({
      title: 'Alta Usuario',
      subTitle: 'Usuario creado con éxito',
      buttons: [{
        text: 'Finalizar',
        role: 'Finalizar',
        handler: () => {
          this.navCtrl.pop()
        }
      },]
    });
    alert.present();
  }








}

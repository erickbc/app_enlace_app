import { ViewMonitoreo } from './../view-monitoreo/view-monitoreo';
import { Component } from '@angular/core';
import { NavController, Loading, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';

/**
 * Generated class for the ViewAccesoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import * as ChartModuleMore from 'highcharts/highcharts-more.js';
import HCSoldGauge from 'highcharts/modules/solid-gauge';

import * as HighCharts from "highcharts";
import { ViewNivelesGenerales } from '../view-NivelesGenerales/view-nivelesGenerales';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Ticket } from '../../providers/Tickets/Ticket';
import { Parameters } from '../../providers/Parameters';

ChartModuleMore(HighCharts);
HCSoldGauge(HighCharts);


@Component({
  selector: 'page-view-tickets',
  templateUrl: 'view-tickets.html',
})
export class ViewTickets {
    loading: Loading;
    subscriptionFirst: any;
    chart1:any;
    arrayItems = []
    tickFirst: any;
    timer=0
    //Opcion para decidir que se selecciono NOC / CARE
    opcion:any
    //NOC o CARE
    opcionSeleccionada:Ticket

    constructor(private nav: NavController,
                private parameters: Parameters,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                public webService: GestionWebServices,
                public  params:NavParams) {
                //Asign Information about tickets
                console.log("constructor - ViewTickets");
                this.opcionSeleccionada = new Ticket();

                this.opcion = params.data.opcion
                this.timer = this.parameters.timer
                this.cargando();
    }

    f = new Date()

    mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")

    //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log("ionViewWillLeave - ViewTickets");
      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log("ionViewWillEnter - ViewTickets");
      let firstTimer = TimerObservable.create(this.timer, this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        //Se trae al metodo que actualiza la pantalla
        //Muy generico el tema , pero el tema de las graficas es muy manual
        this.updateInformation(false);
      });
    }

    updateInformation(online){

      console.log("updateInformation - ViewTickets");

      //Se manda a traer el service que trae informacion de los tickets
      this.webService.getTickets().subscribe((dataI) => {
        if (dataI.status==200) {
          let arrayTickets=dataI.arrayElements
          //Como no podemos sustituir el objeto que vive en la session de la vista
          //Tenemos que actualizar cada dato
          if (this.opcion == 0){
            this.opcionSeleccionada = arrayTickets[0]
          }else{
            this.opcionSeleccionada = arrayTickets[1]
          }

          //Sacar la suma
          let suma = parseInt(this.opcionSeleccionada.totalVencidos) + parseInt(this.opcionSeleccionada.totalXVencer) + parseInt(this.opcionSeleccionada.totalAbiertos)
          //Obtener los Datos
          let data = this.getUpdateInformation(suma,parseInt( this.opcionSeleccionada.totalVencidos),parseInt( this.opcionSeleccionada.totalXVencer),parseInt( this.opcionSeleccionada.totalAbiertos))
          //Actualizar el grafico, los datos se actualizan manualmente porque estan vivos en la session

          this.chart1.series[0].update( data );
        }
        else {
          if(online){
            this.mostrarError(dataI);
            this.nav.pop();
          }
        }
      },
      error => {
        if(online){
          this.mostrarError(error);
          this.nav.pop();
        }
      });


    }

    ionViewDidLoad(){
      console.log("ionViewDidLoad - ViewTickets");

      this.webService.getTickets().subscribe((data) => {
        if(data.status==200){
          if (this.opcion == 0){
            this.opcionSeleccionada = data.arrayElements[0]
          }else{
            this.opcionSeleccionada = data.arrayElements[1]
          }
          //Se Calcula la suma
          let suma = parseInt(this.opcionSeleccionada.totalVencidos) + parseInt(this.opcionSeleccionada.totalXVencer) + parseInt(this.opcionSeleccionada.totalAbiertos)
          //Se obtienen los datos
          let dataG = this.getUpdateInformation(suma,parseInt(this.opcionSeleccionada.totalVencidos),parseInt(this.opcionSeleccionada.totalXVencer),parseInt(this.opcionSeleccionada.totalAbiertos))
          //Se diseña la grafica  NOC O CARE
          this.graphicPerformance(dataG,"grafica")
          this.loading.dismiss();
        }
        else {
          this.mostrarError(data);
          this.nav.pop();
        }
      },
      error => {
        this.mostrarError(error);
        this.nav.pop();
      });



    }

    //Seleccion NOC o CARE , y se corrobora que exista la grafica
    //e informacion para controlar el error
    seleccion(){
      console.log("seleccion - ViewTickets");

      if (this.opcionSeleccionada.status){
        this.nav.push(ViewNivelesGenerales,this.opcionSeleccionada.nombre);
      }
    }


    cargando() {
      console.log("cargando - ViewTickets");

      this.loading = this.loadingCtrl.create({
        content: 'Espere por favor...',
        dismissOnPageChange: false
      });
      this.loading.present();
    }
    //Metodo para traer un diccionario para el chart <<reutilizable>>
    getUpdateInformation(suma,totalVencidos,totalXVencer,totalAbiertos){
      console.log("getUpdateInformation - ViewTickets");

      let porcentajeVencidos = (totalVencidos*100)/suma;
      let porcentajeXVencer = (totalXVencer*100)/suma;
      let porcentajeAbiertos = (totalAbiertos*100)/suma;

      return {
          name: 'Lelim',
          colorByPoint: true,
          data: [{
              name: 'Abiertos',
              y: porcentajeAbiertos,
              sliced: false,
              selected: true,
              color: "#66CB00"
          }, {
              name: 'Por Vencer',
              color: "#F19600",
              y: porcentajeXVencer
          }, {
              name: 'Vencidos',
              color: "#C80B0C",
              y: porcentajeVencidos
          }]
      }

    }


    graphicPerformance(data,nameId){
     console.log("graphicPerformance - ViewTickets");

      this.chart1 = HighCharts.chart(nameId,
      {
        chart: {
          plotBackgroundColor: null,

          backgroundColor:'rgba(255, 255, 255, 0.0)',

          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie',
          size:10
        },
        credits:{
          enabled:false
          },
          title: {
          text: "",
          align: 'center',
          verticalAlign: 'middle',
          y: 40
      },
        tooltip: {
            enabled:false,
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: false
              },
              showInLegend: false
            }
        },
        series: [{
          name: 'Lelim',
          colorByPoint: true,
          data: [data]
      }]
      });

      this.chart1.series[0].update( data );

    }


    selectMonitoreo(){
      console.log("selectMonitoreo - ViewTickets");

      this.nav.push(ViewMonitoreo)
    }
    mostrarError(data) {
      console.log("mostrarError - ViewTickets");

      this.loading.dismiss();

      let alert = this.alertCtrl.create({
        title: 'Error '+data.status,
        subTitle: data.statusText,
        buttons: ['OK']
      });
      alert.present()
    }

}

import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'view-altaIncidente.html'
})
export class ViewAltaIncidente {

  listaUsuarios= []
  nombreUsuario = "El Julion"
  grupoUsuario = "Cumbion"
  contraseniaUsuario = "12345"
  numeroLogin = "12345"

  tabBarElement: any;

  solicitante = "Seleccione Solictante"

  constructor(private alertCtrl: AlertController,public navCtrl: NavController) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

  }


  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }


cancelar(){
  this.navCtrl.pop()
}


alta() {
  let alert = this.alertCtrl.create({
    title: 'Alta Usuario',
    subTitle: 'Usuario creado con éxito',
    buttons: [{
      text: 'Finalizar',
      role: 'Finalizar',
      handler: () => {
        this.navCtrl.pop()
      }
    },]
  });
  alert.present();
}

seleccionSolicitante(){
  let prompt = this.alertCtrl.create({
    title: 'Seleccion Solicitante',
    inputs : [
    {
      type:'radio',
      label:'Solicitante A',
      value:'A'
    },
    {
      type:'radio',
      label:'Solicitante B',
      value:'B'
    },
    {
    type:'radio',
    label:'Solicitante C',
    value:'C'
  },
  ],
    buttons : [
    {
        text: "Cancelar",
        handler: data => {
        }
    },
    {
        text: "Seleccionar",
        handler: data => {
          this.solicitante="Erick Blanco"
        }
    }]});
    prompt.present();
}


seleccionCategoria(){
  let prompt = this.alertCtrl.create({
    title: 'Seleccion Categoria',
    inputs : [
    {
      type:'radio',
      label:'Servicio Estrategico',
      value:'A'
    },
    {
      type:'radio',
      label:'Servicio Telefonia',
      value:'B'
    },
    {
    type:'radio',
    label:'Servicio Terceros',
    value:'C'
  },
  ],
    buttons : [
    {
        text: "Cancelar",
        handler: data => {
        }
    },
    {
        text: "Seleccionar",
        handler: data => {
        }
    }]});
    prompt.present();
}


seleccionSubCategoria(){
  let prompt = this.alertCtrl.create({
    title: 'Seleccion Sub-Categoria',
    inputs : [
    {
      type:'radio',
      label:'Enlace Dedicados',
      value:'A'
    },
    {
      type:'radio',
      label:'Internet',
      value:'B'
    },
    {
    type:'radio',
    label:'Lan to Lan',
    value:'C'
  },
  ],
    buttons : [
    {
        text: "Cancelar",
        handler: data => {
        }
    },
    {
        text: "Seleccionar",
        handler: data => {
        }
    }]});
    prompt.present();
}
seleccionFallas(){
  let prompt = this.alertCtrl.create({
    title: 'Seleccion Falla Incidente',
    inputs : [
    {
      type:'radio',
      label:'Bajo Ancho Banda',
      value:'A'
    },
    {
      type:'radio',
      label:'Falla Total',
      value:'B'
    },
    {
    type:'radio',
    label:'Intermitencia',
    value:'C'
  },
  ],
    buttons : [
    {
        text: "Cancelar",
        handler: data => {
        }
    },
    {
        text: "Seleccionar",
        handler: data => {
        }
    }]});
    prompt.present();
}

}

import { ViewMonitoreo } from './../view-monitoreo/view-monitoreo';
import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { NavController, Loading, AlertController, LoadingController, Events } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { FCM } from '../../../node_modules/@ionic-native/fcm';
import { Parameters } from '../../providers/Parameters';
import { SessionVariables } from '../../providers/Session/SessionVariables';



@Component({
  selector: 'page-view-acceso',
  templateUrl: 'view-acceso.html',
})
export class ViewAcceso {
  loginForm:any;
  loading: Loading;
  credencialesRegistro = { usuario: '', contrasenia: '' , tokenDevice:'' };
  par: Parameters;

  constructor(private nav: NavController,
              private auth: GestionWebServices,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              public http: Http,
              public events: Events,
              private fcm: FCM,
           
            ) {
              console.log("constructor - ViewAcceso")
              /*
              this.fcm.getToken().then(token=>{
                console.log("=====IFNROMATION ABOUT TOKEN IN ACCESS=====");
                console.log(token);
                this.credencialesRegistro.tokenDevice = token;
                console.log(this.credencialesRegistro.tokenDevice);
                console.log("=====IFNROMATION ABOUT TOKEN IN ACCESS=====");
              })*/

  }

  
  //Acceso, se envian las credenciales de acceso user y password
  public acceso() {
    console.log("acceso - ViewAcceso")
    this.cargando()
    this.auth.acceso(this.credencialesRegistro).subscribe(data => {
      if (data.status==200) {
        this.events.publish('user:login', data.user.tipoUsuario);
        this.events.publish('statusBar:changeColor');
        this.events.publish('user:token', this.credencialesRegistro.usuario);
        this.nav.push(ViewMonitoreo);
      }else {
        if(data.error == "E001"){
          this.mostrarError("Usuario y/o Password incorrecto");
        }else if(data.error == "E002"){
          this.mostrarError("Dispositivo no registrado");
        }else if(data.error == "E003"){
          this.mostrarError("Usuario inactivo");
        }else if(data.error == "E004"){
          this.mostrarError("Usuario bloqueado, contactar al administrador");
        }
      }
    },
    error => {
      this.mostrarError(error);
    });
  }

//Loading - Se muestra en pantalla - Cargando
cargando() {
  console.log("cargando - ViewAcceso")
  this.loading = this.loadingCtrl.create({
    content: 'Espere por favor...',
    dismissOnPageChange: true
  });
  this.loading.present();
}
//Se muestra error en pantalla
mostrarError(data) {
  console.log("mostrarError - ViewAcceso")
  this.loading.dismiss();
  let alert = this.alertCtrl.create({
    title: 'Error ',
    subTitle: data,
    buttons: ['OK']
  });
  alert.present()
}

}

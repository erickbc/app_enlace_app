import { Component } from '@angular/core';
import { Tabs, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { Estadisticos } from '../../providers/Estadisticos';

/**
 * Generated class for the ViewAvisoLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'view-listaAlarmas',
  templateUrl: 'view-listaAlarmas.html',
})
export class ViewListaAlarmas{

  loading: Loading;


  items = [1,2,4];

  tab:Tabs;
  parametro:String;
  listaAlarmas =[];
  itemsCopy =[];
  vacio:String;
  alarma:String;
  tipoAlarma:string;
  searchTerm: string = '';
  constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private loadingCtrl: LoadingController,
        public webService: GestionWebServices,
        private alertCtrl: AlertController,
        public estadisticos:Estadisticos

      ) {
        console.log("constructor -ViewListaAlarmas ");
        this.tab = this.navCtrl.parent;
        this.parametro=navParams.get('parametro');
        this.tipoAlarma=navParams.get('alarma');
        console.log("abrirListaAlarmas - ViewGraficaGenerica");


  }

      //Busca en el arreglo de items por "Ubicacion"
      serachInArray (array, value) {
        console.log('serachInArray-ViewListaInfraestructura');
        return array.filter(function (object) {
            let source = object.idAlarma.toLowerCase()
            let query = value.toLowerCase()
            return source.includes(query);
        });
      };

      //Controla la busqueda del front
  searchItems(){
    console.log('searchItems-ViewListaAlarmas');
    var  q = this.searchTerm;
    this.listaAlarmas = this.itemsCopy;
    if (!q) {
      this.listaAlarmas = this.itemsCopy;
      return;
    }

    console.log('searchItems-ViewListaAlarmas');
    this.listaAlarmas = this.serachInArray(this.listaAlarmas,q) 
    console.log('searchItems-ViewListaAlarmas');

  }
  controlResponse(kindDevice,data){
    if(data.length > 0){
      this.listaAlarmas=data;
      this.itemsCopy = this.listaAlarmas; 
    }
    else{
      if (kindDevice=="ROUTER")
        this.vacio="NO EXISTEN ALARMAS EN ROUTER EN ESTE MOMENTO";
      else if (kindDevice=="SWITCHS")
        this.vacio="NO EXISTEN ALARMAS EN SWITCHS EN ESTE MOMENTO";
      else if (kindDevice=="COLECTORES")
        this.vacio="NO EXISTEN ALARMAS EN COLECTORES EN ESTE MOMENTO";
      else if (kindDevice=="OLT")
        this.vacio="NO EXISTEN ALARMAS EN OLT EN ESTE MOMENTO";
    }
  }
  callApiSaludRed(information){
    this.webService.getFiltroAlarmasSaludRed(information).subscribe(data => {
      this.controlResponse(this.alarma,data)
    },
    error => {
    });
  }
  callApiSaludTotal(kindDevice){
    if (kindDevice=="ROUTER")
      this.webService.getListaAlarmasRouter().subscribe(data => {
        this.controlResponse(this.alarma,data)
      },
      error => {
      });
    else if (kindDevice=="SWITCHS")
      this.webService.getListaAlarmasSwitchs().subscribe(data => {
        this.controlResponse(this.alarma,data)
      },
      error => {
      });
    else if (kindDevice=="COLECTORES")
      this.webService.getListaAlarmasColectores().subscribe(data => {
        this.controlResponse(this.alarma,data)
      },
      error => {
      });
    else if (kindDevice=="OLT")
      this.webService.getListaAlarmasOlt().subscribe(data => {
        this.controlResponse(this.alarma,data)
      },
      error => {
      });
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad -ViewListaAlarmas ");
    var para=this.parametro
    let information = {tipoEquipo:"",tipoAlarma:""}
    switch (para) {
      case "1":
          this.alarma="ROUTER";
          information.tipoEquipo =  this.alarma[0]
          information.tipoAlarma =  this.tipoAlarma
          if(this.tipoAlarma==undefined)
            this.callApiSaludTotal(this.alarma);
          else
            this.callApiSaludRed(information)
         
        break;
      case "2" :
        this.alarma="SWITCHS";
        information.tipoEquipo =  this.alarma[0]
        information.tipoAlarma =  this.tipoAlarma
        if(this.tipoAlarma==undefined)
          this.callApiSaludTotal(this.alarma);
        else
          this.callApiSaludRed(information)
        break;
      case "3":
        this.alarma="COLECTORES";
        information.tipoEquipo =  this.alarma[0]
        information.tipoAlarma =  this.tipoAlarma
        if(this.tipoAlarma==undefined)
          this.callApiSaludTotal(this.alarma);
        else
          this.callApiSaludRed(information)
        break;
      case "4":
        this.alarma="OLT";
        information.tipoEquipo =  this.alarma[0]
        information.tipoAlarma =  this.tipoAlarma
        if(this.tipoAlarma==undefined)
         this.callApiSaludTotal(this.alarma);
        else
          this.callApiSaludRed(information)
        break;

    }
  }


  cargando() {
    console.log("cargando -ViewListaAlarmas ");

    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  mostrarError(text) {
    console.log("mostrarError -ViewListaAlarmas ");

    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present()
  }



}

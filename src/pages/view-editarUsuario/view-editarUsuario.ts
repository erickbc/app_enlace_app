import { GestionWebServices } from './../../providers/GestionWebServices';
import { Component } from '@angular/core';
import { NavController, NavParams, Events, AlertController, App } from 'ionic-angular';
import { Password } from '../../providers/Utils/Password';
import jsSHA from 'jssha';
import { CommonUtilsProvide } from '../../providers/Utils/CommonUtilsProvide';

@Component({
  selector: 'page-home',
  templateUrl: 'view-editarUsuario.html'
})
export class ViewEditarUsuario {

  userNameUsuario = ""
  nombreUsuario = ""
  apellidoUsuario = ""
  organizacionUsuario = ""
  idUsuario = ""
  permisoAdministrador = false
  permisoAlarmas = false
  tipoUsuario = false
  usuario:any
  updatePassword = false
  //Fields to control de hide of visible password
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  //Control the user of password
  contraseniaUsuario = null
  UIID = null
  
  constructor(public navCtrl: NavController,
      public webService: GestionWebServices,
      public  params:NavParams,
      public events: Events,
      private alertCtrl: AlertController,
      private encrypt:CommonUtilsProvide,
      public app: App,
    ) {
        console.log(" constructor - ViewEditarUsuario ")
        this.usuario = params.data.usuario
        this.organizacionUsuario = this.usuario.organizacion
        this.userNameUsuario = this.usuario.user.username
        this.nombreUsuario = this.usuario.user.first_name
        this.apellidoUsuario = this.usuario.user.last_name
        this.idUsuario = this.usuario.user.id
        
        if (this.usuario.tipoUsuario == "2001"){
          this.tipoUsuario = true
        }else{
          this.tipoUsuario = false
        }
  }
  //Controlar la visibilidad de la contraseña
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  updatePass(){
    let alert = this.alertCtrl.create({
      title: 'Contraseña',
      subTitle: '¿Estas seguro de que deseas generar otra contraseña?',
      buttons: [{
        text: 'Cancelar',
        role: 'Cancelar',
        handler: () => {
        }
      },{
        text: 'Aceptar',
        role: 'Aceptar',
        handler: () => {
          this.updatePassword =  !this.updatePassword
          this.contraseniaUsuario =  new Password().password_generator();
        }
      },]
    });
    alert.present();
  }
  actualizar(){
        let alert = this.alertCtrl.create({
        title: 'Dispositivo',
        subTitle: '¿Deseas asociar este dispositivo a esta cuenta?',
        buttons: [{
          text: 'Cancelar',
          role: 'Cancelar',
          handler: () => {
            this.updateUser()
          }
        },{
          text: 'Aceptar',
          role: 'Aceptar',
          handler: () => {
          
            this.UIID = this.generateUUID();
            this.updateUser()
          }
        },]
      });
      alert.present()
    }


  updateUser(){
    let data = {}
    if (this.updatePassword){
          data = {
            nombre:this.nombreUsuario,
            apellidoP:this.apellidoUsuario,
            organizacion:this.organizacionUsuario,
            reportes_alarmas:this.permisoAlarmas,
            tipo_usuario:this.tipoUsuario,
            uuid:this.UIID,
            contrasenia:this.encrypt.encryptPayload(this.contraseniaUsuario)
        }
    } else{
        data = {
          nombre:this.nombreUsuario,
          apellidoP:this.apellidoUsuario,
          organizacion:this.organizacionUsuario,
          reportes_alarmas:this.permisoAlarmas,
          tipo_usuario:this.tipoUsuario,
          uuid:this.UIID
        }
    }
    if (this.UIID){
      this.updateUserWithKeyChain(data)
    }else{
      this.updateUserWithoutKeyChain(data)
    }
  }

  updateUserWithKeyChain(data){
    this.webService.updateUsuarioWithKeyChain(this.idUsuario,data).subscribe(data => {
      if(data.status == true){
        let alert = this.alertCtrl.create({
          title: 'Actualización Usuario',
          subTitle: 'Usuario actualizado con éxito',
          buttons: [{
            text: 'Aceptar',
            role: 'Aceptar',
            handler: () => {
              this.usuario.organizacion = this.organizacionUsuario
              this.usuario.user.username = this.userNameUsuario
              this.usuario.user.first_name = this.nombreUsuario
              this.usuario.user.last_name = this.apellidoUsuario
              this.usuario.statusUser = "S002"
              if (!this.tipoUsuario){
                this.usuario.tipoUsuario = "2002"
              }else{
                this.usuario.tipoUsuario = "2001"
              }
              this.navCtrl.pop()
            }
          },]
        });
        alert.present();
      }else{
        let mensaje = ""
        if(data.error == "C001" || data.error == "C002"){
           mensaje = "Consulte al administrador del sistema"
        }
        let alert = this.alertCtrl.create({
          title: 'Error Actualización Usuario',
          subTitle: mensaje,
          buttons: [{
            text: 'Finalizar',
            role: 'Finalizar',
            handler: () => {
              
              this.navCtrl.pop()
            }
          },]
        });
        alert.present();
      }
    },
    error => {
    });
  }

  updateUserWithoutKeyChain(data){
    this.webService.updateUsuario(this.idUsuario,data).subscribe(data => {
    
      if(data.status == true){
        let alert = this.alertCtrl.create({
          title: 'Actualización Usuario',
          subTitle: 'Usuario actualizado con éxito',
          buttons: [{
            text: 'Aceptar',
            role: 'Aceptar',
            handler: () => {
              
              this.usuario.organizacion = this.organizacionUsuario
              this.usuario.user.username = this.userNameUsuario
              this.usuario.user.first_name = this.nombreUsuario
              this.usuario.user.last_name = this.apellidoUsuario
              this.usuario.statusUser = "S002"
              
              if (!this.tipoUsuario){
                this.usuario.tipoUsuario = "2002"
              }else{
                this.usuario.tipoUsuario = "2001"
              }
    
              this.navCtrl.pop()
            }
          },]
        });
        alert.present();
      }else{
        let mensaje = ""
        if(data.error == "C001" || data.error == "C002"){
           mensaje = "Consulte al administrador del sistema"
        }
        let alert = this.alertCtrl.create({
          title: 'Error Actualización Usuario',
          subTitle: mensaje,
          buttons: [{
            text: 'Finalizar',
            role: 'Finalizar',
            handler: () => {
              
              this.navCtrl.pop()
            }
          },]
        });
        alert.present();
  
      }
      
    },
    error => {
     
    });

  }
  
  cancelar(){
    this.navCtrl.pop()
  }

  generateUUID(){
    let date = new Date();
    let components = [
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds()
    ];

    let id = components.join("");
    let shaObj = new jsSHA("SHA-256", "TEXT");
      shaObj.update(id);
    let hash = shaObj.getHash("HEX");
    return hash
  }
  
  alertaUpdateUUID(){
    return  this.alertCtrl.create({
      title: 'Dispositivo',
      subTitle: '¿Deseas asociar este dispositivo a esta cuenta?',
      buttons: [{
        text: 'Cancelar',
        role: 'Cancelar',
        handler: () => {
        }
      },{
        text: 'Aceptar',
        role: 'Aceptar',
        handler: () => {
          this.UIID = this.generateUUID();
        }
      },]
    });
  }

}

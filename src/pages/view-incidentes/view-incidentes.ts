import { ViewAltaIncidente } from './../view-altaIncidente/view-altaIncidente';
import { ViewEditarIncidente } from './../view-editarIncidente/view-editarIncidente';
import { ViewBusquedaIncidente } from './../view-busquedaIncidentes/view-busquedaIncidentes';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'view-incidentes.html'
})
export class ViewIncidente {

  data: Array<{numeroIncidente: string, 
               numeroTicket: string, 
               date:string,
               solicitante:string,
               descripcion:string,
               usurioFinal:string,
               fechaApertura:string,
               categoria:string,
               ultimaModificacion:string,
               showDetails: boolean}> = [];

  constructor(public navCtrl: NavController) {
    for(let i = 0; i < 10; i++ ){
      this.data.push({
          numeroIncidente: String(i),
          numeroTicket: 'TFE-D-VPN3106',
          date:"13/Oct/16",
          solicitante:"N/A",
          descripcion:"N/A",
          usurioFinal:"N/A",
          fechaApertura:"N/A",
          categoria:"N/A",
          ultimaModificacion:"N/A",

          showDetails: false
        });
    }
  }

  detalle(data) {
    if (data.showDetails) {
        data.showDetails = false;
    } else {
        data.showDetails = true;
    }
  }

  busqueda() {
    console.log("Busqueda")
    this.navCtrl.push(ViewBusquedaIncidente);
  }

  seleccionCelda(){
    console.log("Editar Incidente")
    this.navCtrl.push(ViewEditarIncidente);
  }

  agregar(){
    console.log("Editar Incidente")
    this.navCtrl.push(ViewAltaIncidente);

  }
}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'view-detalleInformes.html'
})
export class ViewDetalleInformes {

  constructor(public navCtrl: NavController) {

  }

}

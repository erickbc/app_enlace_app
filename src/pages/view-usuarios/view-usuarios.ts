import { ViewIncidente } from './../view-incidentes/view-incidentes';
import { ViewAltaUsuario } from './../view-altaUsuario/view-altaUsuario';
import { ViewEditarUsuario } from './../view-editarUsuario/view-editarUsuario';
import { Component } from '@angular/core';
import { NavController, AlertController, Loading, LoadingController, Events } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'view-usuarios.html'
})
export class ViewUsuarios {

  loading: Loading;
  nombreUsuario: string = '';

  listaUsuarios= []
  listaUsuariosCopia= []
  idUserLogin = ""
  constructor(private alertCtrl: AlertController,
              public navCtrl: NavController,
              public webService: GestionWebServices,
              private loadingCtrl: LoadingController,
              public events: Events,
              public storage:Storage

            ) {


                storage.get('idUser').then((val) => {
                  this.idUserLogin = val
                });


                events.subscribe('user:created', (user, time) => {
                  // user and time are the same arguments passed in `events.publish(user, time)`
                  console.log('Welcome', user, 'at', time);
                  //let nameUser =  + " " +user.apellidoUsuario
                  console.log("Nombre de Usuario")
                  console.log(user.userName)
                  let u = {username:user.userName, first_name: user.nombreUsuario, last_name:user.apellidoUsuario, id:user.id}
                  let dayaJson = {user:u,organizacion: user.organizacion.nombre,tipoUsuario:user.tipoUsuario,statusUser:'S002'}
                  console.log("Usuario to Add")
                  console.log(u)
                  this.listaUsuarios.unshift(dayaJson)
                });

                events.subscribe('user:edited', (user, time) => {
                  // user and time are the same arguments passed in `events.publish(user, time)`
                  console.log('Welcome', user, 'at', time);

                  var userFind = this.listaUsuariosCopia.indexOf(user)
                  console.log("Usuario Encontrado")
                  console.log(userFind)



                });

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewAvisoLegalPage');
    this.loadUsuarios()
  }



  public loadUsuarios() {
    this.cargando()
    this.webService.getAllUsers().subscribe(data => {
      console.log("Information Data")
      console.log(data)
      if (data.status) {
        this.listaUsuarios = data.users
        this.listaUsuariosCopia = data.users
        console.log("LIsta de Usuarios");
        console.log(this.listaUsuarios);
      }else {
        this.mostrarError("No hay informacion disponible");
      }
    },
    error => {
      this.mostrarError("Error al cargar los archivos");
    });
  }



mostrarError(text) {
  this.loading.dismiss();

  let alert = this.alertCtrl.create({
    title: 'Error',
    subTitle: text,
    buttons: ['OK']
  });
  alert.present()
}


  cargando() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  busquedaUsuarios() {
    var nombre = this.nombreUsuario;
    if (!nombre) {
      this.listaUsuarios = this.listaUsuariosCopia;
      return;
    }
    this.listaUsuarios = this.listaUsuariosCopia.filter(x => x.user.username.indexOf(nombre) !== -1)
  }

  editarUsuario(key){

    console.log("====Editar Usuario====")
    console.log(key)
    console.log(this.listaUsuariosCopia)
    let usuario = this.listaUsuariosCopia.filter(x => x.user.id == key)
    let param = {usuario: usuario[0]};
    console.log("Data to Edit")
    console.log(param)
    this.navCtrl.push(ViewEditarUsuario,param);
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave - ViewMonitoreo");
    this.navCtrl.swipeBackEnabled = true;
  }
  //Cada vez que entra a la pantalla se inicia el timer que mantiene
  //actualizado la pantalla
  ionViewWillEnter(){
    console.log("ionViewWillEnter - ViewMonitoreo");
    this.navCtrl.swipeBackEnabled = false;
  }

  creaUsuario(){
    this.navCtrl.push(ViewAltaUsuario);
  }

  retorno(){
    this.navCtrl.setRoot(ViewIncidente);
  }

  eliminarUsuario(key){

      console.log("User to Delete")
      console.log(key)
      let alert = this.alertCtrl.create({
        title: 'Eliminar Usuario',
        subTitle: 'Estas seguro de que deseas eliminar el usuario',
        buttons: [{
          text: 'Cancelar',
          role: 'Cancelar',
          handler: () => {
          }
        },{
          text: 'Aceptar',
          role: 'Aceptar',
          handler: () => {
            let usuario = this.listaUsuariosCopia.filter(x => x.user.id == key)[0]
            var index = this.listaUsuariosCopia.indexOf(usuario);
            if (index > -1) {
              this.listaUsuariosCopia.splice(index, 1);
            }

            this.webService.deleteUsuario(key).subscribe(data => {
              console.log("Information Data - After Delete User")
              console.log(data)

            },
            error => {

            });


          }
        },]
      });
      alert.present();



  }

}

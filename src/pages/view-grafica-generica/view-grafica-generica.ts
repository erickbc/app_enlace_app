import { Estadisticos } from './../../providers/Estadisticos';
import { Component } from '@angular/core';
import { NavController, NavParams, Config, App, LoadingController, Loading, AlertController } from 'ionic-angular';


//------Highcharts----------

import * as ChartModuleMore from 'highcharts/highcharts-more.js';
import HCSoldGauge from 'highcharts/modules/solid-gauge';
import * as HighCharts from "highcharts";
import { ViewListaAlarmas } from '../view-listaAlarmas/view-listaAlarmas';
import { Tabs } from 'ionic-angular/navigation/nav-interfaces';
import { Parameters } from '../../providers/Parameters';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';
ChartModuleMore(HighCharts);
HCSoldGauge(HighCharts);

//-------
@Component({
  selector: 'page-contact',
  templateUrl: 'view-grafica-generica.html'
})
export class ViewGraficaGenerica {

    //Graficas
    graficaPrincipal:any;
    graficaCPU:any;
    graficaMemoria:any;

    //Timer
    timer=0
    subscriptionFirst: any;
    tickFirst: any;

    //Loading
    loading: Loading;


    //Ids Graficas
    grafica1={id:"",title:"Disponibilidad"};
    grafica2={id:"",title:"CPU"};
    grafica3={id:"",title:"Memoria"};
    //Identificador de que device estamos manejando
    parametro:any;
    //Device
    device :any

    constructor(public navCtrl: NavController,
                public estadisticos:Estadisticos,
                public params: NavParams,
                public config: Config,
                public webService: GestionWebServices,
                public app:App,
                private parameters :Parameters,
                private loadingCtrl: LoadingController,
                private alertCtrl: AlertController

    ) {
      console.log("constructor - ViewGraficaGenerica");
      this.parametro = params.data
      this.timer = this.parameters.timer;
      //Idenficiamos que Dispositivo estamos posicionados
      // y sobre ese vaciamos datos
      switch(this.parametro) {
        case "1":
            this.grafica1.id = "principalR"
            this.grafica2.id = "cpuR"
            this.grafica3.id = "memoryR"
            this.device = estadisticos.router
            break;
        case "2":
            this.grafica1.id = "principalS"
            this.grafica2.id = "cpuS"
            this.grafica3.id = "memoryS"
            this.device = estadisticos.switch
            break;
        case "3":

            this.grafica1.id = "principalC"
            this.grafica2.id = "cpuC"
            this.grafica3.id = "memoryC"
            this.device = estadisticos.colector
            break;

        case "4":
            this.grafica1.id = "principalO"
            this.grafica2.id = "cpuO"
            this.grafica3.id = "memoryO"
            this.device = estadisticos.olt
            break;
      }
    }
    //Metodo para mostrar la pantalla cargando
    cargando() {
      console.log("cargando - ViewGraficaGenerica");
      this.loading = this.loadingCtrl.create({
        content: 'Espere por favor...',
        dismissOnPageChange: true
      });
      this.loading.present();
    }

   //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log("ionViewWillLeave - ViewGraficaGenerica");

      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log("ionViewWillEnter - ViewGraficaGenerica");

      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        this.updateInformation()
      });
    }

    updateInformation(){
      console.log("updateInformation - ViewGraficaGenerica");

      //this.cargando()
      //Llamamos al WS para actualizar la informacion dependiendo del device
      this.webService.getEstadisticas().subscribe(data2 => {
        if(data2.status==200){
            switch(this.parametro) {
            case "1":
                this.device = data2.estadisticos.router
                break;
            case "2":
                this.device =data2.estadisticos.switch
                break;
            case "3":
                this.device = data2.estadisticos.colector
                break;
            case "4":
                this.device = data2.estadisticos.olt
                break;
            }
            //Extraemos los valores del device y sobre ello actualziamos la grafica
            let data = this.getInformacionGraficaPrincipal(this.device.disponibilidad)
            this.graficaPrincipal.series[0].update( data );

            data = this.getInformacionSecundaria(this.device.cpu)
            this.graficaCPU.series[0].update( data );

            data = this.getInformacionSecundaria(this.device.memoria)
            this.graficaMemoria.series[0].update( data );
            //this.loading.dismiss();
        }
        else {
            //this.mostrarError(data2);
            //this.saliendo()
        }
      },
      error => {
        //this.mostrarError(error);
        //this.saliendo()
      });
    }


    ionViewDidLoad(){
      console.log("ionViewDidLoad - ViewGraficaGenerica");

      //Cargamos informacion Grafica Principal
      let data = this.getInformacionGraficaPrincipal(this.device.disponibilidad)
      this.principalChart(this.device.disponibilidad,data,this.grafica1.id);
      //Cargamos informacion Grafica CPU
      data = this.getInformacionSecundaria(this.device.cpu)
      this.graficaCPUChart(this.device.cpu,data,this.grafica2.id);
      //Cargamos informacion Grafica Memoria
      data = this.getInformacionSecundaria(this.device.memoria)
      this.graphicMemoriaChart(this.device.memoria,data,this.grafica3.id);
    }
    //Retornamos a la pagina anterior
    saliendo(){
      console.log("saliendo - ViewGraficaGenerica");
      var t: Tabs = this.navCtrl.parent;
      t.select(0,{},false);
    }

    
    home(){
    console.log('saliendo-ViewResume');
    this.app.getRootNav().setRoot(ViewMonitoreo)
    //this.navCtrl.setRoot(ViewMonitoreo)
  }
    //Abrimos Lista Alarmas
    abrirListaAlarmas(){
      console.log("abrirListaAlarmas - ViewGraficaGenerica");
      this.navCtrl.push(ViewListaAlarmas,{parametro : this.parametro});
    }

    getInformacionGraficaPrincipal(disponible){
      console.log("getInformacionGraficaPrincipal - ViewGraficaGenerica");

      let noDisponible = Math.abs( 100-disponible);

      return {
        type: 'pie',
        name: 'valor',
        innerSize: '50%',
        data: [
            {
                name: 'No Disponible',
                y: noDisponible,
                color: '#bf1521',
                dataLabels: {
                    enabled: false
                }
            },
            {
                name: 'Disponible',
                y: disponible,
                color: '#39bf21',
                dataLabels: {
                    enabled: false
                }
            }
        ]
      }



    }

     
    
    retorno(){
        this.navCtrl.setRoot(ViewMonitoreo);
      }
  
    getInformacionSecundaria(disponible){
      console.log("getInformacionSecundaria - ViewGraficaGenerica");

      let noDisponible = Math.abs( 100-disponible);
      return {
        type: 'pie',
        name: 'valor',
        innerSize: '50%',
        data: [
            {
              name: 'No Disponible',
              y: disponible,
              color: '#bf1521',
              dataLabels: {
                  enabled: false
              }
          },
          {
              name: 'Disponible',
              y: noDisponible,
              color: '#39bf21',
              dataLabels: {
                  enabled: false
              }
          }
        ]
      }
    }

    principalChart(information,data,id) {
      console.log("principalChart - ViewGraficaGenerica");

      this.graficaPrincipal = HighCharts.chart(id,
          {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                backgroundColor:'rgba(255, 255, 255, 0.0)'
            },
            credits:{
              enabled:false
            },
            title: {
                text: information+'%',
                align: 'center',
                verticalAlign: 'middle',
                style: { "color": "#7ED321"},
                y: 80
            },
            tooltip: {
                enabled:false,
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    borderWidth:0,
                    states:{
                    hover:{
                            animation:{
                            duration:50
                        },
                        brightness:0.1,
                        enabled:true,
                        halo:{
                            attributes:undefined,
                            opacity:0.25,
                            size:0,
                        }
                      }
                    },
                    dataLabels: {
                        enabled: false,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        }
                    },
                    startAngle: -100,
                    endAngle: 100,
                    center: ['50%', '75%']
                }
            },
            series: [{
              type: 'pie',
              name: 'valor',
              innerSize: '50%',
              data: [
                data
              ]
          }]
        });

        this.graficaPrincipal.series[0].update( data );
    }

    //Dibuja Grafica CPU
    graficaCPUChart(information,data,nameId){
      console.log("graficaCPUChart - ViewGraficaGenerica");


        this.graficaCPU = HighCharts.chart(nameId,

        {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              hight:300,
              backgroundColor:'rgba(255, 255, 255, 0.0)'
          },
          credits:{
            enabled:false
          },
          title: {
            text: "",
            align: 'center',
            verticalAlign: 'middle',
            y: 40
        },
          tooltip: {
              enabled:false,
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
              borderWidth:0,
              states:{
                      hover:{
                              animation:{
                              duration:50
                          },
                          brightness:0.1,
                          enabled:true,
                          halo:{
                              attributes:undefined,
                              opacity:0.25,
                              size:0,
                          }
                        }
                      },
                  dataLabels: {
                      enabled: false,
                      distance: -50,
                      style: {
                          fontWeight: 'bold',
                          color: 'white'
                      }
                  },
                  startAngle: -90,
                  endAngle: 360,
                  center: ['50%', '75%']
              }
          },
          series: [{
            type: 'pie',
            name: 'valor',
            innerSize: '50%',
            data: [
                    data
            ]
        }]
      });
        this.graficaCPU.series[0].update( data );
    }
    //Dibuja Grafica Memoria
    graphicMemoriaChart(information,data,nameId){
      console.log("graphicMemoriaChart - ViewGraficaGenerica");

        this.graficaMemoria = HighCharts.chart(nameId,

        {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              hight:300,
              backgroundColor:'rgba(255, 255, 255, 0.0)'
          },
          credits:{
            enabled:false
          },
          title: {
            text: "",
            align: 'center',
            verticalAlign: 'middle',
            y: 40
        },
          tooltip: {
              enabled:false,
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
              borderWidth:0,
              states:{
                      hover:{
                              animation:{
                              duration:50
                          },
                          brightness:0.1,
                          enabled:true,
                          halo:{
                              attributes:undefined,
                              opacity:0.25,
                              size:0,
                          }
                        }
                      },
                  dataLabels: {
                      enabled: false,
                      distance: -50,
                      style: {
                          fontWeight: 'bold',
                          color: 'white'
                      }
                  },
                  startAngle: -90,
                  endAngle: 360,
                  center: ['50%', '75%']
              }
          },
          series: [{
              type: 'pie',
              name: 'valor',
              innerSize: '50%',
              data: [data
              ]
          }]
      });

      this.graficaMemoria.series[0].update( data );

    }
    mostrarError(data) {
      console.log("mostrarError - ViewGraficaGenerica");

        //this.loading.dismiss();

        let alert = this.alertCtrl.create({
          title: 'Error '+data.status,
          subTitle: data.statusText,
          buttons: ['OK']
        });
        alert.present()
      }

}

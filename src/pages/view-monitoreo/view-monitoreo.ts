import { TabsInfraPage } from './../tabsInfra/tabsInfra';
import { ViewTickets } from './../view-tickets/view-tickets';
import { Component } from '@angular/core';
import { NavController, Loading, AlertController, LoadingController, Events } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { GestionWebServices } from '../../providers/GestionWebServices';

/**
 * Generated class for the ViewAccesoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import * as ChartModuleMore from 'highcharts/highcharts-more.js';
import HCSoldGauge from 'highcharts/modules/solid-gauge';

import * as HighCharts from "highcharts";
import { Parameters } from '../../providers/Parameters';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ViewNivelesGenerales } from '../view-NivelesGenerales/view-nivelesGenerales';
import { ViewResumeInfra } from '../view-resumeInfra/view-resumeInfra';
import { ViewNivelesDetalle } from '../view-NivelesDetalle/view-nivelesDetalle';

ChartModuleMore(HighCharts);
HCSoldGauge(HighCharts);


@Component({
  selector: 'page-view-monitoreo',
  templateUrl: 'view-monitoreo.html',
})
export class ViewMonitoreo {
  alarmaTotalSalud=0;
  alarmaTotalInfraestructura=0;
  timer=0
  subscriptionFirst: any;
  tickFirst: any;
  loading: Loading;
  sumaNoc = 0
  sumaCare = 0

  //BANDERA TEST
  bandera = false
  bandera2 = false
  arrayNoc = []
  arrayCare = []
  constructor(private nav: NavController,
    public events: Events,
    public webService: GestionWebServices,
    private alertCtrl: AlertController,
    private parameters :Parameters,
    private loadingCtrl: LoadingController,
  ) {
    console.log("constructor - ViewMonitoreo");
    this.timer = this.parameters.timer;

  }


   //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log("ionViewWillLeave - ViewMonitoreo");
      this.nav.swipeBackEnabled = true;

      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log("ionViewWillEnter - ViewMonitoreo");
      this.nav.swipeBackEnabled = false;

      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        this.alarmasTotalesSalud(false);
        this.loadingInformationNOC(false)
        this.loadingInformationCare(false);
        this.alarmasTotalesTickets(false);
        this.alarmasTotalesInfraestructura(false);
      });
    }

    update(){

    }

  cargando() {
    console.log("cargando - ViewMonitoreo");

    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }


  loadingInformationNOC(online){
    console.log('loadingInformationNOC ViewNivelesGenerales');

    this.webService.getNocGeneral().subscribe(data => {
      if (data.status==200) {
        this.arrayNoc = []
        let sumFO = 0
          sumFO += data.nocGeneral.frontOffice.alta.vencidos
          sumFO += data.nocGeneral.frontOffice.critica.vencidos
          sumFO += data.nocGeneral.frontOffice.media.vencidos

        let sumBO = 0
          sumBO += data.nocGeneral.backOffice.alta.vencidos
          sumBO += data.nocGeneral.backOffice.critica.vencidos
          sumBO += data.nocGeneral.backOffice.media.vencidos

        
        this.arrayNoc.push({data:data.nocGeneral.frontOffice,suma:sumFO})
        this.arrayNoc.push({data:data.nocGeneral.backOffice,suma:sumBO})
      }
      else {
        if (online){
          this.mostrarError(data);
         }
      }
    },
    error => {
      if (online){
        this.mostrarError(error);
       }
    });
  }

  loadingInformationCare(online){

    
    this.webService.getCareGeneral().subscribe(data => {
      if (data.status==200) {
        this.arrayCare = []
        //this.arrayCare.push(data.careGeneral.monitoreo)
       // this.arrayCare.push(data.careGeneral.estrategico)
        //this.arrayCare.push(data.careGeneral.gobierno)

        let sumCM = 0
          sumCM += data.careGeneral.monitoreo.alta.vencidos
          sumCM += data.careGeneral.monitoreo.critica.vencidos
          sumCM += data.careGeneral.monitoreo.media.vencidos

        let sumCE = 0
          sumCE += data.careGeneral.estrategico.alta.vencidos
          sumCE += data.careGeneral.estrategico.critica.vencidos
          sumCE += data.careGeneral.estrategico.media.vencidos

        let sumCG = 0
          sumCG += data.careGeneral.gobierno.alta.vencidos
          sumCG += data.careGeneral.gobierno.critica.vencidos
          sumCG += data.careGeneral.gobierno.media.vencidos

          this.arrayCare.push({data:data.careGeneral.monitoreo,suma:sumCM})
          this.arrayCare.push({data:data.careGeneral.estrategico,suma:sumCE})
          this.arrayCare.push({data:data.careGeneral.gobierno,suma:sumCG})
        
      }
      else {
        if(online){
          this.mostrarError(data);
        }
      }

    },
    error => {
      if(online){
        this.mostrarError(error);
      }
    });
  }


  ionViewDidLoad(){
    console.log("ionViewDidLoad - ViewMonitoreo");

    this.cargando();
    this.alarmasTotalesSalud(true);
    this.alarmasTotalesTickets(true);
    this.loadingInformationNOC(true)
    this.loadingInformationCare(true);
    this.alarmasTotalesInfraestructura(true);
  }
  selectTickets(){
    console.log("selectTickets - ViewMonitoreo");
    this.nav.pop()
  }
  selectMonitoreo(){}
  seleccionRed(){
    console.log("seleccionRed - ViewMonitoreo");
    let tipoUser = {"typeUser":2001,"alarmas":true}
    //this.events.publish('user:login', 2001);
    this.nav.push(TabsPage,tipoUser);
  }
  seleccionInfraestructura(){
    console.log("seleccionInfraestructura - ViewMonitoreo");
    let tipoUser = {"typeUser":2001,"alarmas":true}
    //this.events.publish('user:login', 2001);
    this.nav.push(ViewResumeInfra,tipoUser);
  }
  seleccionTicket(tipo){
    console.log("seleccionTicket - ViewMonitoreo");
    //let opcion = {"opcion":tipo}
    //this.nav.push(ViewTickets,opcion);
    if (tipo == "0"){
      this.bandera =!this.bandera
      //this.nav.push(ViewNivelesGenerales,"NOC");
    }else{
      //this.nav.push(ViewNivelesGenerales,"CARE");
      this.bandera2 =!this.bandera2
    }
   
    
  }
  seleccionTicketNocCare(item,type){
    console.log("seleccionTicketNocCare - ViewMonitoreo");

    if(type===0){
      let d = {nombre:item.data.nombre,tipo:"NOC"}
      this.nav.push(ViewNivelesDetalle,d);
    }else{
      let d = {nombre:item.data.nombre,tipo:"CARE"}
      this.nav.push(ViewNivelesDetalle,d);
    }

   
    
  }

  alarmasTotalesSalud(online){
    console.log("alarmasTotalesSalud - ViewMonitoreo");
    this.webService.getEstadisticas().subscribe(data => {
      if(data.status==200){
        this.alarmaTotalSalud=0;
        //console.log("Information Data")
        //console.log(estadisticos)
        this.alarmaTotalSalud+=parseInt(data.estadisticos.router.down)+parseInt(data.estadisticos.router.fallaGestion)+parseInt(data.estadisticos.router.fallaOptica)
        this.alarmaTotalSalud+=parseInt(data.estadisticos.switch.down)+parseInt(data.estadisticos.switch.fallaGestion)+parseInt(data.estadisticos.switch.fallaOptica)
        this.alarmaTotalSalud+=parseInt(data.estadisticos.colector.down)+parseInt(data.estadisticos.colector.fallaGestion)+parseInt(data.estadisticos.colector.fallaOptica)
        this.alarmaTotalSalud+=parseInt(data.estadisticos.olt.down)+parseInt(data.estadisticos.olt.fallaGestion)+parseInt(data.estadisticos.olt.fallaOptica)
        //console.log("alarmaTotalSalud"+this.alarmaTotalSalud)
      }
      else {
        if (online){
         this.mostrarError(data);
        }
      }
    },
    error => {
      if (online){
        this.mostrarError(error);
      }
    });
  }
  alarmasTotalesInfraestructura(online){
    console.log("alarmasTotalesInfraestructura - ViewMonitoreo");


    this.webService.getEstadisticasInfra().subscribe(data => {
      if(data.status==200){
        this.alarmaTotalInfraestructura=0;
        data.estadisticos.listaGenerica.forEach(equipo => {
          this.alarmaTotalInfraestructura+=equipo.totalAlarmas
        });
      }
      else {
        if (online){

          this.mostrarError(data);
          this.ionViewWillLeave();
        }
        //this.saliendo()
      }
    },
    error => {
      if (online){

        this.mostrarError(error);
        this.ionViewWillLeave();
      }
      //this.saliendo()
    });
  }
  mostrarError(data) {
    console.log("mostrarError - ViewMonitoreo");

    let alert = this.alertCtrl.create({
      title: 'Error '+data.status,
      subTitle: data.statusText,
      buttons: ['OK']
    });
    alert.present()
  }

  alarmasTotalesTickets(online){
    console.log("alarmasTotalesTickets - ViewMonitoreo");

    this.webService.getTickets().subscribe(data => {
      if(data.status==200){
        //Sacar la suma
        this.sumaNoc = parseInt(data.arrayElements[0].totalVencidos)
        this.sumaCare = parseInt(data.arrayElements[1].totalVencidos)
        this.loading.dismiss();
      }
      else {
        if (online){

        this.mostrarError(data);
        }
      }
    },
    error => {
      if (online){

      this.mostrarError(error);
      }
    });
  }
}

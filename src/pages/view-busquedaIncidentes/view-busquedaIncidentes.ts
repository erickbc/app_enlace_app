import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'view-busquedaIncidentes.html'
})
export class ViewBusquedaIncidente {
  opciones = "";
  constructor(public navCtrl: NavController) {
    this.opciones="todos"
  }

  cancelar(){
    this.navCtrl.pop()
  }


}

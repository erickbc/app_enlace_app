import { ViewListaInfraestructura } from './../view-listaInfraestructura/view-listaInfraestrctura';
import { NavParams, ModalController } from 'ionic-angular';



import { Component } from '@angular/core';

import { ViewAvisoLegalUser } from '../view-aviso-legalUser/view-aviso-legalUser';
import { ViewResumeInfra } from '../view-resumeInfra/view-resumeInfra';


@Component({
  templateUrl: 'tabsInfra.html'
})
export class TabsInfraPage {
/*
  tabInformes = ViewInformes;
  tabIncidentes = ViewIncidente;
  tabDetalleinformes = ViewDetalleInformes;
*/
tabResume =  ViewResumeInfra;
tabAviso = ViewAvisoLegalUser;

tabGeneric = ViewListaInfraestructura;
alarmas = false

typeTabRouter = "1";
typeTabSwitch = "2";
typeTabColector = "3";
typeTabOTL = "4";

  constructor(public params:NavParams,
    public modalCtrl: ModalController) {
    this.alarmas = params.data.alarmas

  }


}

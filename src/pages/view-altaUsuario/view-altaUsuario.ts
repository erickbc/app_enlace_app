import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import jsSHA from 'jssha'
import { Password } from '../../providers/Utils/Password';
import { CommonUtilsProvide } from '../../providers/Utils/CommonUtilsProvide';

@Component({
  selector: 'page-home',
  templateUrl: 'view-altaUsuario.html'
})
export class ViewAltaUsuario {

  listaUsuarios= []
  usuarioLogin = ""
  nombreUsuario = ""
  apellidoUsuario = ""
  organizacion = ""
  contraseniaUsuario = ""
  UIID = ""
  administrador = false
  permisoAlarmas = false
  //Para controlar la vista o ocultacion del password
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
 
  constructor(private alertCtrl: AlertController,
              public navCtrl: NavController,
              public events: Events,
              private webS: GestionWebServices,
              private encrypt:CommonUtilsProvide) {
              console.log("constructor - ViewAltaUsuario")
              //Se genera un password aleatorio
              this.contraseniaUsuario =  new Password().password_generator();
              //Se genera un UIID aleatorio
              this.UIID = this.generateUUID();
  }

  //Para controlar la vista o ocultacion del password
  hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  //Se genera un UIID aleatorio
  generateUUID(){
    let date = new Date();
    let components = [
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds()
    ];

    let id = components.join("");
    let shaObj = new jsSHA("SHA-256", "TEXT");
      shaObj.update(id);
    let hash = shaObj.getHash("HEX");
    return hash
  }
  //Cancela la vista
  cancelar(){
    this.navCtrl.pop()
  }

  //Se da de alta el usuario
  alta() {
    console.log("alta - ViewAltaUsuario")
      let adm = "2002"
      if (this.administrador)
        adm = "2001"
  
      let information = {
        'username': this.usuarioLogin,
        'nombreUsuario': this.nombreUsuario,
        'apellidoUsuario': this.apellidoUsuario,
        'password': this.encrypt.encryptPayload(this.contraseniaUsuario),
        'organizacion': this.organizacion,
        'tipoUsuario': adm,
        'alarmas': this.permisoAlarmas,
        'uuid':this.UIID
      };
  
      this.webS.agregaUsuario(information).subscribe(data => {
        if (data.status) { 
          let alert = this.alertCtrl.create({
            title: 'Alta Usuario',
            subTitle: 'Usuario creado con éxito',
            buttons: [{
              text: 'Finalizar',
              role: 'Finalizar',
              handler: () => {
                console.log('User created!')
                this.events.publish('user:created', data.user, Date.now());
                this.navCtrl.pop()
              }
            },]
          });
          alert.present();
        }else {
          let mensaje = ""
          if (data.error == "C001"){
              mensaje = "Ya existe un usuario, rectifique información"
          }else if(data.error == "C002" || data.error == "C003"){
             mensaje = "Consulte al administrador del sistema"
          }
          let alert = this.alertCtrl.create({
            title: 'Error Alta Usuario',
            subTitle: mensaje,
            buttons: [{
              text: 'Finalizar',
              role: 'Finalizar',
              handler: () => {
                this.navCtrl.pop()
              }
            },]
          });
          alert.present();
        }
      },error => {
      });
  }
}

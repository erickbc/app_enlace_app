import { Component } from '@angular/core';
import { NavController, reorderArray } from 'ionic-angular';


import * as ChartModuleMore from 'highcharts/highcharts-more.js';
import HCSoldGauge from 'highcharts/modules/solid-gauge';
import * as HighCharts from "highcharts";
ChartModuleMore(HighCharts);
HCSoldGauge(HighCharts);

@Component({
  selector: 'page-home',
  templateUrl: 'view-informes.html'
})
export class ViewInformes {

  items = [];
  grafica1={id:"container",title:"Estado de los servicios"};
  grafica2={id:"containerTwo",title:"Consumo de ancho de banda"};
  grafica3={id:"containerThree",title:"Tickets"};
  reorder = false
  textEditar = "Editar"
  constructor(public navCtrl: NavController) {
    this.items.push(this.grafica1);
    this.items.push(this.grafica2);
    this.items.push(this.grafica3);

  }

  reorderItems(indexes) {
    this.items = reorderArray(this.items, indexes);
  }

  editar(){
    this.reorder=!this.reorder
    if (this.reorder){
      this.textEditar="OK"
    }else{
      this.textEditar="Editar"
    }

  }
  ionViewDidLoad(){

    HighCharts.chart('container', {
      chart: {
        type: 'solidgauge'
      },
      title: {
          text: ' ',
          style: {
              fontSize: '16px'
          }
      },
      tooltip: {
          borderWidth: 0,
          backgroundColor: 'none',
          shadow: false,
          style: {
              fontSize: '14px'
          },
     pointFormat: '{series.name}<br><span style=\font-size:1em; color: {point.color}; font-weight: bold\>{point.y} puntas</span>',
          positioner: function (labelWidth) {
              return {
                  x: 150 - labelWidth / 2,
                  y: 90
              };
          }
      },
      pane: {
          startAngle: 0,
          endAngle: 360,
          background: [{
             outerRadius: '112%',
             innerRadius: '88%',
             backgroundColor: HighCharts.Color(HighCharts.getOptions().colors[1]).setOpacity(0.3).get(),
             borderWidth: 0
          }, {
             outerRadius: '87%',
             innerRadius: '63%',
             backgroundColor: HighCharts.Color(HighCharts.getOptions().colors[1]).setOpacity(0.3).get(),
             borderWidth: 0
          }]
      },
      yAxis: {
          min: 0,
          max: 100,
          lineWidth: 0,
          tickPositions: []
      },
      plotOptions: {
          solidgauge: {
              dataLabels: {
                  enabled: false
              },
              linecap: 'round',
              stickyTracking: false,
              rounded: true,
             animation: false
          }
      },
      series: [{
         name: 'Online',
         borderColor: HighCharts.getOptions().colors[2],
         data: [{
             color: '#0A98CF',
             radius: '112%',
             innerRadius: '88%',
             y:   50
         }]
      }, {
         name: 'Offline',
         borderColor: HighCharts.getOptions().colors[3],
         data: [{
             color: '#FA7902',
             radius: '87%',
             innerRadius: '63%',
             y:   12
         }]
      }]
  });


  HighCharts.chart('containerTwo', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
      text: ' ',
      y: 47,
      style: {
        fontSize: '16px'
       }
    },
    tooltip: {
        pointFormat: '{series.name} <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true,
           animation: false
        }
    },
    legend: {
           align: 'center',
           x: 20,
           verticalAlign: 'bottom',
           y: 1,
           floating: false,
           borderColor: '#CCC',
           borderWidth: 0,
           shadow: false
     },
    exporting:{
        buttons:{
            contextButton:{
                enabled: false
            }
        }
    },
    series: [{
        name: ' ',
        colorByPoint: true,
        data: [{
            name: 'Uso óptimo',
            y:  12  ,
            color: '#38C98D'
        }, {
            name: 'Al limite',
            y:  16  ,
            color: '#FAB320'
        },{
            name: 'Al máximo',
            y:  23  ,
            color: '#FA7902'
        }]
        }]
    });

  HighCharts.chart('containerThree', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        spacingTop: -100
    },
    title: {
        text: 'Disponibilidad<br>Febrero',
        align: 'center',
        verticalAlign: 'middle',
        y: 90,
    style: {
        fontSize: '16px'
    }
    },
    tooltip: {
        pointFormat: '{series.name} <b>{point.percentage:.1f}%</b>'
    },
    exporting:{
        buttons:{
            contextButton:{
                enabled: false
            }
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -20,
                style: {
                    fontWeight: 'bold',
                    color: 'black',
                    fontSize: '10px'
                },
                x:30,
                y:-5
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
           animation: false
        }
    },
    series: [{
    type: 'pie',
    name: ' ',
    innerSize: '50%',
    data: [
    {
    y:   12  ,
    name: 'Disponible',
    color: '#01A4A4'
    }, {
    y:   88  ,
    name: 'No disponible',
    color: '#FAB320'
    }]
    }]
});


  }



}

import { Estadisticos } from './../../providers/Estadisticos';
import { Component } from '@angular/core';
import { NavController, NavParams, Config, App, LoadingController, Loading, AlertController } from 'ionic-angular';
import { Tabs } from 'ionic-angular/navigation/nav-interfaces';
import { Parameters } from '../../providers/Parameters';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';

@Component({
  selector: 'page-contact',
  templateUrl: 'view-detalleEspecificoNoCare.html'
})

export class ViewDetalleEspecifico
 {
    //Timer to check the status
    timer=0
    subscriptionFirst: any;
    tickFirst: any;
    //Loading
    loading: Loading;
    //Identificador de que device estamos manejando
    parametro:any
    prioridad:any; //Nivel: media,alta,baja
    categoria:any;//Categoria, de acuerdo a cada item
    status:any;//Abierto, Cerrado, xVencer 1 Vencidos, 2 Por Vencer 3 Abiertos
    estado:any;//Abierto, Cerrado, xVencer 1 Vencidos, 2 Por Vencer 3 Abiertos Solo guardamos el numero 1,2,3
    tipo:string;
    statusColor:any; //Color Barra de acuerdo al status
    total:any;//Total "tickts"
    area:any; //TIPO TOTAL FrontOfficce, Back, Monitoreo ,Gobierno
    tipoVista:any; //Totales o con errores.
    //======================Items to copy to the search
    items: any = [];
    nombre:String;

    constructor(public navCtrl: NavController,
                public estadisticos:Estadisticos,
                public params: NavParams,
                public config: Config,
                public webService: GestionWebServices,
                public app:App,
                private parameters :Parameters,
                private loadingCtrl: LoadingController,
                private alertCtrl: AlertController
    ) {
      console.log('constructor-ViewListaInfraestructura');
      
      this.prioridad = params.data.level;
      this.categoria = params.data.item.categoria;
      this.area = params.data.area;
      this.estado = params.data.status
      this.tipo = params.data.tipo


      if (this.estado==2) {
        this.status = "VENCIDOS"
        this.total = params.data.item.vencidos; 
        this.statusColor = "tituloRojo"
      } else if(this.estado==1) {
        this.status = "POR VENCER"
        this.total = params.data.item.xVencer; 
        this.statusColor = "tituloAmarillo"
      }else{
        this.status = "ABIERTOS"
        this.total = params.data.item.abiertos; 
        this.statusColor = "tituloVerde"
      }


      if (this.prioridad=="crítica") {
          this.prioridad = "C"
      } else if(this.prioridad=="media") {
        this.prioridad = "M"
      }else{
        this.prioridad = "A"
      }

      if (this.area=="Front Office") {
        this.area = "F"
      } else if(this.area=="Back Office"){
        this.area = "B"
      }else{
        this.area = this.quitaacentos(this.area.replace("Care",""))
      }

      if (this.tipo=="NOC") {
        this.tipo = "N"
      } else{
        this.tipo = "C"
      }


    
      this.timer = this.parameters.timer;
      this.cargando()
    }
    

     quitaacentos(s) {
      var r=s.toLowerCase();
                  r = r.replace(new RegExp(/\s/g),"");
                  r = r.replace(new RegExp(/[àáâãäå]/g),"a");
                  r = r.replace(new RegExp(/[èéêë]/g),"e");
                  r = r.replace(new RegExp(/[ìíîï]/g),"i");
                  r = r.replace(new RegExp(/ñ/g),"n");                
                  r = r.replace(new RegExp(/[òóôõö]/g),"o");
                  r = r.replace(new RegExp(/[ùúûü]/g),"u");
                  
       return r;
      }

    //Metodo para mostrar la pantalla cargando
    cargando() {
      console.log('cargando-ViewListaInfraestructura');
      this.loading = this.loadingCtrl.create({
        content: 'Espere por favor...',
        dismissOnPageChange: true
      });
      this.loading.present();
    }


    loadInformation(online){

      console.log('loadInformation-ViewListaInfraestructura');
      let information = {
        "categoria":this.categoria,
        "prioridad":this.prioridad,
        "area":this.area,
        "estado":this.estado+"",
        "tipo":this.tipo+"",
      };
        

        console.log(information);

      
      this.webService.getInformationNocCare(information).subscribe(data => {
        if(data.status){
          console.log(data)
          this.items = data.data.detalle;
          console.log(this.items)
          if(this.items.length==0){this.saliendo()}

        }
        else {
          if(online){
            this.mostrarError(data);
            this.ionViewWillLeave();
            this.saliendo()
          }
        }
      },
      error => {
        if(online){
          this.mostrarError(error);
          this.ionViewWillLeave();
          this.saliendo()
        }
      });



    }
    
    ionViewDidLoad() {
      console.log('ionViewDidLoad-ViewListaInfraestructura');
      //Carga de Informacion
      this.loadInformation(true);
      this.loading.dismiss();
    }

    retorno(){
      this.navCtrl.setRoot(ViewMonitoreo);
    }

    //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log('ionViewWillLeave-ViewListaInfraestructura');
      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log('ionViewWillEnter-ViewListaInfraestructura');
      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        this.loadInformation(false);
      });
    }

    //Retornamos a la pagina anterior
    saliendo(){
      console.log('saliendo-ViewListaInfraestructura');
      this.navCtrl.pop()
    }

    mostrarError(data) {
      console.log('mostrarError-ViewListaInfraestructura');
      let alert = this.alertCtrl.create({
        title: 'Error '+data.status,
        subTitle: data.statusText,
        buttons: ['OK']
      });
      alert.present()
    }

}

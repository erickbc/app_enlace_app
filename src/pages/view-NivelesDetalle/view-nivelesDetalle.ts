import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Parameters } from '../../providers/Parameters';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';
import { ViewDetalleEspecifico } from '../view-detalleEspecificoNoCare/view-detalleEspecificoNoCare';

/**
 * Generated class for the ViewAvisoLegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'view-nivelesDetalle',
  templateUrl: 'view-nivelesDetalle.html',
})
export class ViewNivelesDetalle{

  loading: Loading;

  arrayInformation = []


  subscriptionFirst: any;
  tickFirst: any;
  timer = 0;
  nombre:String;
  tipo:String;
  constructor(
    
        public navCtrl: NavController,
        public navParams: NavParams,
        private loadingCtrl: LoadingController,
        public webService: GestionWebServices,
        private alertCtrl: AlertController,
        public  params:NavParams,
        private parameters : Parameters

      ) {
        console.log('constructor ViewNivelesDetalle');

      this.timer = this.parameters.timer;
      this.nombre = params.data.nombre
      this.tipo = params.data.tipo
      

  }
  loadInformation(online){
    console.log('loadInformation ViewNivelesDetalle');

    if (this.nombre.includes("Care")){
      this.loadingInformationDetalleCare(online)
    }else if(this.nombre.includes("Back")){
      this.loadingInformationDetalleBack(online)
    }else if(this.nombre.includes("Front")){
      this.loadingInformationDetalleFront(online)
    }
  }

  specificDetail(level,status,item){
    console.log('specificDetail ViewNivelesDetalle');

    /*
    status = 
    1 = Vencidos
    2 = xVencer
    3= Abiertos
    */

      let d = {level:level,item:item,status:status,area:this.nombre,tipo:this.tipo}

      this.navCtrl.push(ViewDetalleEspecifico,d);
    
 
  
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewNivelesDetalle');
    this.loadInformation(true);


  }
  retorno(){
    this.navCtrl.setRoot(ViewMonitoreo);
  }
  
  //Cada vez que sale de la pantalla se muere el timer que mantiene
  //actualizado la pantalla
  ionViewWillLeave() {
    this.subscriptionFirst.unsubscribe();
  }
  //Cada vez que entra a la pantalla se inicia el timer que mantiene
  //actualizado la pantalla
  ionViewWillEnter(){
    let firstTimer = TimerObservable.create( this.timer,  this.timer);
    this.subscriptionFirst = firstTimer.subscribe(t => {
      this.tickFirst = t;
      this.loadInformation(false);
    });
  }
  cargando() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  
  mostrarError(data) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: "No es posible mostrar la información en estos momentos.",
      buttons: ['OK']
    });
    alert.present()
  }


  loadingInformationDetalleCare(online){
    if(online){
      this.cargando();
    }
   
    this.webService.getCareDetalle().subscribe(data => {
      if (data.status==200){
        this.arrayInformation=[]
        if (this.nombre == "Care Monitoreo"){
          let n = {nivel:"crítica",detalle:data.careDetalle.monitoreo.critica}
          this.arrayInformation.push(n);
          n = {nivel:"alta",detalle:data.careDetalle.monitoreo.alta}
          this.arrayInformation.push(n);
          n = {nivel:"media",detalle:data.careDetalle.monitoreo.media}
          this.arrayInformation.push(n);
        }else if(this.nombre == "Care Estratégico"){
          let n = {nivel:"crítica",detalle:data.careDetalle.estrategico.critica}
          this.arrayInformation.push(n);
          n = {nivel:"alta",detalle:data.careDetalle.estrategico.alta}
          this.arrayInformation.push(n);
          n = {nivel:"media",detalle:data.careDetalle.estrategico.media}
          this.arrayInformation.push(n);
        }else if(this.nombre == "Care Gobierno"){
          let n = {nivel:"crítica",detalle:data.careDetalle.gobierno.critica}
          this.arrayInformation.push(n);
          n = {nivel:"alta",detalle:data.careDetalle.gobierno.alta}
          this.arrayInformation.push(n);
          n = {nivel:"media",detalle:data.careDetalle.gobierno.media}
          this.arrayInformation.push(n);
        }
        if(online){
          this.loading.dismiss();
        }
      }
      else {
        if(online){
        this.mostrarError(data);
        this.navCtrl.pop()
        }
      }

    },
    error => {
      if(online){
        this.mostrarError(error);
        this.navCtrl.pop()
      }
    });
  }

  loadingInformationDetalleBack(online){
    if(online){
      this.cargando();
    }
    this.webService.getNocDetalleBack().subscribe(data => {
      if(data.status==200){
        this.arrayInformation=[]
        let n = {nivel:"crítica",detalle:data.careDetalle.backOffice.critica}
        this.arrayInformation.push(n);
        n = {nivel:"alta",detalle:data.careDetalle.backOffice.alta}
        this.arrayInformation.push(n);
        n = {nivel:"media",detalle:data.careDetalle.backOffice.media}
        this.arrayInformation.push(n);
        if(online){
          this.loading.dismiss();
        }
      }
      else {
        if(online){
          this.mostrarError(data);
          this.navCtrl.pop()
        }
      }
    },
    error => {
      if(online){
        this.mostrarError(error);
        this.navCtrl.pop()
      }
    });
  }


  loadingInformationDetalleFront(online){
    if(online){
      this.cargando();
    }
    this.webService.getNocDetalleFront().subscribe(data => {
      if(data.status==200){
        this.arrayInformation=[]
        let n = {nivel:"crítica",detalle:data.nocDetalleFront.frontOffice.critica}
        this.arrayInformation.push(n);
        n = {nivel:"alta",detalle:data.nocDetalleFront.frontOffice.alta}
        this.arrayInformation.push(n);
        n = {nivel:"media",detalle:data.nocDetalleFront.frontOffice.media}
        this.arrayInformation.push(n);

        console.log("LOADING INFORMATION FRONT")
        console.log(this.arrayInformation)
        if(online){
          this.loading.dismiss();
        }
      }
      else {
        if(online){
          this.mostrarError(data);
          this.navCtrl.pop()
        }
      }

    },
    error => {
      if(online){
        this.mostrarError(error);
        this.navCtrl.pop()
      }
    });
  }





}

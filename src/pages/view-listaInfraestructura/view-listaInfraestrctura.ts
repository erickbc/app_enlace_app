import { Estadisticos } from './../../providers/Estadisticos';
import { Component } from '@angular/core';
import { NavController, NavParams, Config, App, LoadingController, Loading, AlertController } from 'ionic-angular';


//------Highcharts----------

import * as ChartModuleMore from 'highcharts/highcharts-more.js';
import HCSoldGauge from 'highcharts/modules/solid-gauge';
import * as HighCharts from "highcharts";
import { Tabs } from 'ionic-angular/navigation/nav-interfaces';
import { Parameters } from '../../providers/Parameters';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { GestionWebServices } from '../../providers/GestionWebServices';
ChartModuleMore(HighCharts);
HCSoldGauge(HighCharts);

//-------
@Component({
  selector: 'page-contact',
  templateUrl: 'view-listaInfraestrctura.html'
})
export class ViewListaInfraestructura {

    //Graficas
    graficaPrincipal:any;
    graficaCPU:any;
    graficaMemoria:any;

    //Timer
    timer=0
    subscriptionFirst: any;
    tickFirst: any;

    //Loading
    loading: Loading;


    //Ids Graficas
    grafica1={id:"",title:"Disponibilidad"};
    grafica2={id:"",title:"CPU"};
    grafica3={id:"",title:"Memoria"};
    //Identificador de que device estamos manejando
    parametro:any;
    //Device
    device :any
    //====TESTIN ITEMS
    items: any = [];
    itemExpandHeight: number = 100;
    nombre:String;
    vacio:String;
    listaInfra =[];

    constructor(public navCtrl: NavController,
                public estadisticos:Estadisticos,
                public params: NavParams,
                public config: Config,
                public webService: GestionWebServices,
                public app:App,
                private parameters :Parameters,
                private loadingCtrl: LoadingController,
                private alertCtrl: AlertController

    ) {
      console.log('constructor-ViewListaInfraestructura');
      this.parametro = params.data;
      this.timer = this.parameters.timer;
      this.cargando()
    }
    //Metodo para mostrar la pantalla cargando
    cargando() {
      console.log('cargando-ViewListaInfraestructura');
      this.loading = this.loadingCtrl.create({
        content: 'Espere por favor...',
        dismissOnPageChange: true
      });
      this.loading.present();
    }


    loadInformation(online){
      var para=this.parametro


      this.webService.getEstadisticasInfraDetalle().subscribe(data => {
        if(data.status == 200){
          this.items = [];
          switch (para) {
            case "1":
                this.nombre="CRÍTICA";
                this.items=data.listasInfra.listaCritica
              break;
            case "2" :
              this.nombre="MEDIA";
              this.items=data.listasInfra.listaMedia
              break;
            case "3":
              this.nombre="BAJA";
              this.items=data.listasInfra.listaBaja
              break;
          }
        }
        else {
          if(online){
            this.mostrarError(data);
            this.ionViewWillLeave();
            this.saliendo()
          }
        }
      },
      error => {
        if(online){
          this.mostrarError(error);
          this.ionViewWillLeave();
          this.saliendo()
        }
      });



    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad-ViewListaInfraestructura');
        this.loadInformation(true);
        this.loading.dismiss();
      }
      expandItem(item){
        console.log('expandItem-ViewListaInfraestructura');
        this.items.map((listItem) => {

            if(item == listItem){
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }
            return listItem;

        });

    }
    //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log('ionViewWillLeave-ViewListaInfraestructura');
      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log('ionViewWillEnter-ViewListaInfraestructura');
      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        this.loadInformation(false);
      });
    }

    //Retornamos a la pagina anterior
    saliendo(){
      console.log('saliendo-ViewListaInfraestructura');
      var t: Tabs = this.navCtrl.parent;
      t.select(0,{},false);
    }

    mostrarError(data) {
      console.log('mostrarError-ViewListaInfraestructura');
      let alert = this.alertCtrl.create({
        title: 'Error '+data.status,
        subTitle: data.statusText,
        buttons: ['OK']
      });
      alert.present()
    }

}

import { Estadisticos } from './../../providers/Estadisticos';
import { Component } from '@angular/core';
import { NavController, NavParams, Config, App, LoadingController, Loading, AlertController } from 'ionic-angular';
import { Tabs } from 'ionic-angular/navigation/nav-interfaces';
import { Parameters } from '../../providers/Parameters';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { GestionWebServices } from '../../providers/GestionWebServices';
import { ViewMonitoreo } from '../view-monitoreo/view-monitoreo';

@Component({
  selector: 'page-contact',
  templateUrl: 'view-listaTotalInfraestrctura.html'
})

export class ViewListaTotalInfraestructura {


    //Timer to check the status
    timer=0
    subscriptionFirst: any;
    tickFirst: any;
    //Loading
    loading: Loading;
    //Identificador de que device estamos manejando
    parametro:any; //Parametro para decidir de que se viene (Shelter,etc)
    tipoVista:any; //Totales o con errores.
    //======================Items to copy to the search
    items: any = [];
    itemsCopy: any = [];
    //====================
    //En caso de que se requiera expandir el acordeon
    itemExpandHeight: number = 100;
    nombre:String;
    //Variable para controlar la busqueda
    searchTerm: string = '';

    constructor(public navCtrl: NavController,
                public estadisticos:Estadisticos,
                public params: NavParams,
                public config: Config,
                public webService: GestionWebServices,
                public app:App,
                private parameters :Parameters,
                private loadingCtrl: LoadingController,
                private alertCtrl: AlertController
    ) {
      console.log('constructor-ViewListaInfraestructura');
      this.parametro = params.data.item;
      this.tipoVista = params.data.tipo;
    
      //This point will be outside , becuase the load of the information will be in other methods
      /*
      if( this.tipoVista == 1){
        this.loadDummiesTotal()
      }else{
        this.loadDummiesError()
      }
      */
      this.timer = this.parameters.timer;
      
    }
    //Busca en el arreglo de items por "Ubicacion"
    serachInArray (array, value) {
      console.log('serachInArray-ViewListaInfraestructura');
      return array.filter(function (object) {
          let source = object.nombre.toLowerCase()
          let query = value.toLowerCase()
          return source.includes(query);
      });
    };
    //Controla la busqueda del front
    searchItems(){
      console.log('searchItems-ViewListaInfraestructura');
      var  q = this.searchTerm;
      this.items = this.itemsCopy;
      if (!q) {
        this.items = this.itemsCopy;
        return;
      }
      this.items = this.serachInArray(this.items,q) 
    }

    //Dar de Alta el Tickets
    darDeAltaTicket(item, event: Event) {
      console.log('darDeAltaTicket-ViewListaInfraestructura');
      event.preventDefault();
      let alert = this.alertCtrl.create({
        title: 'Eliminar Usuario',
        subTitle: 'Estas seguro de que deseas eliminar el usuario',
        buttons: [
          {
          text: 'Cancelar',
          role: 'Cancelar',
          handler: () => {}
          },
          {
          text: 'Aceptar',
          role: 'Aceptar',
          handler: () => {
            item.statusTicket = true,
            //Maybe call a service
            item.numeroDeTicket="654321"
          }
          },
        ]
      });
      alert.present();
    }

    
    
    //Metodo para mostrar la pantalla cargando
    cargando() {
      console.log('cargando-ViewListaInfraestructura');
      this.loading = this.loadingCtrl.create({
        content: 'Espere por favor...',
        dismissOnPageChange: true
      });
      this.loading.present();
    }




    loadInformationEquipos(online){
      let information = {"dispositivo":this.parametro.descripcion}
      
      this.webService.getInformationInfraEquipos(information).subscribe(data => {
        if(data.status){
          
          this.items = [];
          this.items = data.data.listaGenerica;
          this.itemsCopy = this.items
          
          this.loading.dismiss();
          
        }
        else {
          if(online){
            this.mostrarError(data);
            this.ionViewWillLeave();
            this.saliendo()
          }
        }
      },
      error => {
        if(online){
          this.mostrarError(error);
          this.ionViewWillLeave();
          this.saliendo()
        }
      });
    }
  
    loadInformationAlarmas(online){
      let information = {"dispositivo":this.parametro.descripcion}
      this.webService.getInformationInfraAlarmas(information).subscribe(data => {
        if(data.status){
          this.items = [];
          this.items = data.data.listaGenerica;
          this.itemsCopy = this.items
          this.loading.dismiss();
        }
        else {
          if(online){
            this.mostrarError(data);
            this.ionViewWillLeave();
            this.saliendo()
          }
        }
      },
      error => {
        if(online){
          this.mostrarError(error);
          this.ionViewWillLeave();
          this.saliendo()
        }
      });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad-ViewListaInfraestructura');
      //Carga de Informacion
      this.cargando()
      if( this.tipoVista == 1){
        this.loadInformationEquipos(true);
      }else{
        this.loadInformationAlarmas(true);
      }
      
      
    }

    //Controlar el llamado del acordeon   
    prevetSubmit(event) {
      event.srcEvent.stopPropagation();
    }
    

    expandItem(item){
      console.log('expandItem-ViewListaInfraestructura');
      this.items.map((listItem) => {
        if(item == listItem){
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }


 
    
    retorno(){
      this.navCtrl.setRoot(ViewMonitoreo);
    }


    //Cada vez que sale de la pantalla se muere el timer que mantiene
    //actualizado la pantalla
    ionViewWillLeave() {
      console.log('ionViewWillLeave-ViewListaInfraestructura');
      this.subscriptionFirst.unsubscribe();
    }
    //Cada vez que entra a la pantalla se inicia el timer que mantiene
    //actualizado la pantalla
    ionViewWillEnter(){
      console.log('ionViewWillEnter-ViewListaInfraestructura');
      let firstTimer = TimerObservable.create( this.timer,  this.timer);
      this.subscriptionFirst = firstTimer.subscribe(t => {
        this.tickFirst = t;
        
        if( this.tipoVista == 1){
          //this.loadInformationEquipos(false);
        }else{
         // this.loadInformationAlarmas(false);
        }

      });
    }

    //Retornamos a la pagina anterior
    saliendo(){
      console.log('saliendo-ViewListaInfraestructura');
      var t: Tabs = this.navCtrl.parent;
      t.select(0,{},false);
    }

    mostrarError(data) {
      console.log('mostrarError-ViewListaInfraestructura');
      let alert = this.alertCtrl.create({
        title: 'Error '+data.status,
        subTitle: data.statusText,
        buttons: ['OK']
      });
      alert.present()
    }

}
